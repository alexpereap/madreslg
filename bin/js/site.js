// for set preview image on fb post modal
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#fbPreviewImage').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function blockKeyInput( jqueryObject, length ){

     $(jqueryObject).keydown(function(e){

        // keycode 8 = backspace, 9 = tab, 46 = delete, 37 left arrow, 39 right arrow
        if( $(this).val().length >= length && e.keyCode != 8 && e.keyCode != 9 && e.keyCode != 46 && e.keyCode != 37 && e.keyCode != 39 ){
            e.preventDefault();
            return false;
        }
    });
}

$(document).ready(function(){

    // Manual register forms validations
    blockKeyInput( $("input[name='user_document']"), 12 );
    blockKeyInput( $("input[name='user_cellphone']"), 10 );
    blockKeyInput( $("input[name='mom_document']"), 12 );
    blockKeyInput( $("input[name='mom_cellphone']"), 10 );

    // FB copy & paste txt
    /*var fbPreviewMessage = 'Copia y pega el mensaje de arriba y postealo en facebook.';

    $("#fbPreviewMessage").blur(function(){
         if( $(this).val() == '' ){
            $(this).val( fbPreviewMessage );
        }
    });

    $("#fbPreviewMessage").click(function(){

        if( $(this).val() == fbPreviewMessage ){
            $(this).val('');
        }

    });*/

    // checkbox functionality for radios
    $("#radio-1, #radio-2").click(function(e){


       if( $(this).attr('previousValue') == 'false' ){

            $(this).prop('checked',true);
            $(this).attr('previousValue','true');
       } else {

            $(this).prop('checked',false);
            $(this).attr('previousValue','false');
       }

    });

    $(".completed-challenge-img").click(function(e){
        e.preventDefault();
    });

    $(".completed-challenge-img").fancybox();

    // postonFbAction
    $(document).on("click", "#postChallengeOnFb", function(e){
        e.preventDefault();

        // validation for impatients dblclick :@
        if( $(this).attr('disabled') == 'disabled' ){
            return false;
        }
        $(this).attr('disabled','disabled');

        if( $("#fbPreviewMessage").val() == fbPreviewMessage ){
            $("#fbPreviewMessage").val('');
        }

        $(this).attr('disabled','disabled');
        $("#challengeText").val( $("#fbPreviewMessage").val() );
        $("#sendChallengeForm").submit();
    });

    // on facebook post preview modal close
    $('#fbPostPreview').on('hidden.bs.modal', function (e) {
        $("#challengeFile").val('');
    })

    // fileSelected on current challenge
    $(document).on("change", "#challengeFile", function(e){
        ext = $(this).val().split('.');
        ext = ext[ ext.length - 1 ].toLowerCase();
        console.log(ext);

        if( ext != 'png'  && ext != 'jpg' && ext != 'jpeg' ){
            alert("Por favor sube un archivo de extensión jpg, jpeg, png");
            return false;
        }

        // sets preview image
        readURL(this);

        // $("#fbPreviewDescription").html( 'Yo demuestro lo mucho que quiero a Mamá para equiparla con increíbles premios, #YoQuieroAMamáConLG, si tú también quieres participar ingresa en http://equipaamamaconlg.com — con ' + $("#momName").val() );
        $("#fbPreviewDescription").html( 'Cuéntale a todos que estás participando por equipar a tu mamá con lo mejor de LG.' );
        $("#fbPostPreview").modal();
    });

    //upload challenge image handler
    $(document).on("click", "#uploadImg", function(e){
        e.preventDefault();
        $("#challengeFile").click();
    });

    // generates a challenge
    $(document).on("click", "#generateChallenge", function(e){

        e.preventDefault();

        // validation for impatients :@
        if( $(this).attr('disabled') == 'disabled' ){
            return false;
        }

        $(this).attr('disabled','disabled');

        $.ajax({
            url : 'site/ajax_getChallengeByUserID',
            type : 'post',
            success : function(result){
                $("#genChallengeWrapper").fadeOut('slow', function(){
                    $("#genChallengeWrapper").html(result);
                    $("#genChallengeWrapper").fadeIn();
                    $("#generateChallenge").hide();
                    var target = $("#retoLg");

                    $('html, body').animate({
                        scrollTop: $(target).offset().top + 15
                    }, 1000);
                });

                    //}



            }
        });
    });

    //

    $("#submitEngageChallenge").click(function(e){
        e.preventDefault();

        $("#engagementChallengeForm").submit();
    });

    $("#engagementChallengeFile").change(function(){

        // validation

        ext = $(this).val().split('.');
        ext = ext[ ext.length - 1 ].toLowerCase();
        console.log(ext);

        if( ext != 'png'  && ext != 'jpg' && ext != 'jpeg' ){
            alert("Por favor sube un archivo de extensión jpg, jpeg o png");
            return false;
        } else {
            $("#uploadEngChallenge").attr('disabled','disabled');
            $("#engagementChallengeForm").submit();
        }
    });

    // upload engagement challenge
    $("#uploadEngChallenge").click(function(e){

        if( $(this).attr('disabled') == 'disabled' ){
            return false;
        }

        e.preventDefault();
        $("#engagementChallengeFile").click();
    });

    // register validation
    $("#registerForm").bootstrapValidator({
        feedbackIcons: {
           /*valid: 'glyphicon glyphicon-ok',*/
           invalid: 'glyphicon glyphicon-remove',
           validating: 'glyphicon glyphicon-refresh'
       },
       fields : {
            user_firstname: {
             validators: {
                 notEmpty: {
                     message: 'El nombre es requerido'
                 },
                 stringLength: {
                        min: 2,
                        max: 100
                    },
                     regexp: {
                     regexp: /^[A-za-zÁÉÍÓÚáéíóúñÑ\s]+$/,
                     message: 'El nombre solo puede contener letras'
                    }
                }
            },
            user_lastname: {
             validators: {
                 notEmpty: {
                     message: 'El apellido es requerido'
                 },
                 stringLength: {
                        min: 2,
                        max: 100
                    },
                     regexp: {
                     regexp: /^[A-za-zÁÉÍÓÚáéíóúñÑ\s]+$/,
                     message: 'El nombre solo puede contener letras'
                    }
                }
            },
            user_email: {
             validators: {
                 notEmpty: {
                     message: 'El correo electrónico es requerido y no puede estar vacío'
                 },
                 emailAddress: {
                     message: 'El correo electrónico no es valido'
                 }
                     }
            },
            user_email: {
             validators: {
                 notEmpty: {
                     message: 'El correo electrónico es requerido y no puede estar vacío'
                 },
                 emailAddress: {
                     message: 'El correo electrónico no es valido'
                 }
                     }
            },
            user_document: {
                 message: 'El documento no es valido',
                 validators: {
                     notEmpty: {
                         message: 'El documento es requerido y no puede estar vacío'
                     },
                     stringLength: {
                            min: 6,
                            max: 12
                        },
                     regexp: {
                         regexp: /^[0-9]+$/,
                         message: 'El documento solo puede contener números'
                     }
                 }
            },
            user_address:{
             validators: {
                 notEmpty: {
                     message: 'El campo de dirección es requerido y no puede estar vacío'
                 }
             }
            },
            user_city:{
             validators: {
                 notEmpty: {
                     message: 'La ciudad es requerida y no puede estar vacío'
                 },
                 stringLength: {
                        min: 2,
                        max: 20
                    }

             }
            },
             user_cellphone: {
             message: 'El número de telefono celular no es valido',
             validators: {
                notEmpty: {
                     message: 'El número de telefono celular es requerido y no puede estar vacío'
                 },
                 regexp: {
                     regexp: /^[0-9]+$/,
                     message: 'El número de telefono celular solo puede contener números'
                 },
                 stringLength: {
                        min: 10,
                        max: 10
                    }
                }
            },
            mom_name: {
             validators: {
                 notEmpty: {
                     message: 'El nombre es requerido'
                 },
                 stringLength: {
                        min: 2,
                        max: 100
                    },
                     regexp: {
                     regexp: /^[A-za-zÁÉÍÓÚáéíóúñÑ\s]+$/,
                     message: 'El nombre solo puede contener letras'
                    }
                }
            },
            mom_lastname: {
             validators: {
                 notEmpty: {
                     message: 'El apellido es requerido'
                 },
                 stringLength: {
                        min: 2,
                        max: 100
                    },
                     regexp: {
                     regexp: /^[A-za-zÁÉÍÓÚáéíóúñÑ\s]+$/,
                     message: 'El apellido solo puede contener letras'
                    }
                }
            },
            mom_document: {
                 message: 'El documento no es valido',
                 validators: {
                     notEmpty: {
                         message: 'El documento es requerido y no puede estar vacío'
                     },
                     stringLength: {
                            min: 6,
                            max: 12
                        },
                     regexp: {
                         regexp: /^[0-9]+$/,
                         message: 'El documento solo puede contener números'
                     }
                 }
            },
            mom_cellphone: {
             message: 'El número de telefono celular no es valido',
             validators: {
                notEmpty: {
                     message: 'El número de telefono celular es requerido y no puede estar vacío'
                 },
                 regexp: {
                     regexp: /^[0-9]+$/,
                     message: 'El número de telefono celular solo puede contener números'
                 },
                 stringLength: {
                        min: 10,
                        max: 10
                    }
                }
            }
        }
    }).on('success.form.bv', function(e) {

         if( !$("#radio-1").prop('checked') ){
                e.preventDefault();
                $("input[type=submit]").removeAttr('disabled');
                alert("Debes aceptar los términos y condiciones");
         }
    });

    // mom search on register
    if( typeof user_friends != 'undefined' ){

        $( "#search" ).autocomplete({
            minLength : 2,
            source: user_friends,
            select : function(event, ui){

                setTimeout(function(){
                    $("#search").val('');
                     $(".mom-field").removeAttr('readonly');
                     $("#momByFbWrapper").show();
                     $("#momNoFbWrapper").hide();
                }, 1);


                $("#momName").focus();
                $("#momFbId").val( ui.item.id );
                $("#momPicture").val( ui.item.picture );
                $("#momName").val( ui.item.value );
                $("#momNameTxt").text( ui.item.value );

                setTimeout(function(){
                    var press = jQuery.Event("keypress");
                        press.ctrlKey = false;
                        press.which = 97;
                        $("#momName").trigger(press);
                },100);

            },
            change: function(event, ui) {
                if( ui.item == null ){

                    $("#momFbId").val('');
                    $("#momPicture").val('');
                    $("#momByFbWrapper").hide();
                    $("#momNoFbWrapper").show();
                }
            }
        });
    }
});

