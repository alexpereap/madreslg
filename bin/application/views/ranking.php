<!-- BLOCK SECTION: Intro Profile -->
<div class="sg-body sg-intCont container-fluid">
  <section class="row">
    <article class="col-xs-12 col-md-12 col-sm-10 col-md-offset-0 col-sm-offset-1 col-xs-offset-0 toMidVrtcll">
      <img src="imgs/imgIntMom.png" align="left">
      <p class="msgIntro"><span class="ref">Acumula puntos cumpliendo los retos de categoría y los retos diarios,</span> los primeros 6 en el ranking ganarán increíbles productos LG</p>
      <!-- <p class="msgIntro">¡Vuelve pronto para conocer tu posición en el Ranking!</p> -->
    </article>

    <!--INIT TABLE RANKING-->
    <?php if( true ): ?>
    <article class="col-xs-12 col-sm-10 col-md-11 col-xs-offset-0 col-sm-offset-1 col-md-offset-1 intRankArtcl">
      <!--TABLE RANKING LG-->
        <table class="table table-striped table-bordered table-condensed tblRnkLG">
          <thead>
            <tr>
              <th>Posición</th>
              <th>Nombre</th>
              <th>Apellido</th>
              <th>Premio</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($ranking as $key => $value) { ?>
            <tr>
              <td><span><?php echo $value->posicion; ?></span></td>
              <td><?php echo $value->Nombres; ?></td>
              <td><?php echo $value->Apellidos; ?></td>
              <td><?php echo $key > 5 ? '---' : $prizes[$key]; ?></td>
            </tr>
            <?php  }?>
          </tbody>
        </table><!--/.table.tblRnkLG-->
    </article><!--/.intRankArtcl-->
  <?php endif; ?>
  </section><!--/.row-->
</div><!--/.sg-intCont-->

<!--FOOTER MOM´S-->

