<!-- BLOCK SECTION -->
<div class="sg-body sg-homeIntro container">
  <section class="row">
   <!-- ==================================================
    INTRO BANNERS: -->
    <div class="col-xs-12 col-sm-12 col-md-12 netsScrpt">
      <div class="fb-share-button" data-href="http://equipaamamaconlg.com/" data-layout="button_count"></div>
    </div> <!--/.netsScrpt-->

    <div class="col-xs-12 col-sm-5 col-md-5 momHomeFig">
      <img src="imgs/momNameLG.png" id="momNameLG" />
    </div><!--/.momHomeFig-->

    <div class="col-xs-12 col-sm-5 col-md-7 bnnrHomeLikes">
      <img src="imgs/bannerIntroShares.png" />
      <hgroup class="messageMomLG bnnrIntrMsg">
        <h3><strong>Quiere a tu mamá</strong> en <span>Facebook</span> y equípala con lo mejor de <strong>LG</strong></h3>
      </hgroup><!--/.messageMomLG.bnnrIntrMsg-->
         <p>Estamos validando las interacciones de todos los participantes y muy pronto publicaremos los ganadores en nuestras redes.</p>

          <h4 style="  font-family: 'lg_smartbold';
  font-weight: bold;
  color: #b00343;" >
            <strong>¡Gracias a todos por participar y demostrar lo mucho que quieren a Mamá</strong>
          </h4>


        <!--/.nvsBtnsCta-->
    </div><!--/.bnnrHomeLikes-->

  </section><!--/.row-->
</div><!--/.sg-body.sg-homeIntro-->

<!-- MECHANICS BLOCK SECTION -->
<div style="display:none;" class="sg-body sg-homeCont grayPtrnBg container-offTp">
  <section class="row mechsGameHm">
    <div class="col-xs-12 col-md-12">
      <h1 class="titlBrdrLft">Mecánica</h1></div>
    <ul class="row mchnstLst">
      <li class="col-xs-12 col-sm-4 col-md-4">
        <span class="num">1.</span><strong>Ingresa tus datos personales</strong> junto con los datos de tu mamá.</li>
      <li class="col-xs-12 col-sm-4 col-md-4">
        <span class="num">2.</span>Te daremos retos en los cuales tendrás que demostrar <strong>tu cariño por mamá.</strong></li>
      <li class="col-xs-12 col-sm-4 col-md-4">
        <span class="num">3.</span><strong>Toma una foto cumpliendo el reto</strong> y publícalo a través de nuestra aplicación.</li>
    </ul>
    <p>Además si cumples los retos diarios puedes ganar una <strong><a target="_BLANK" href="http://www.lg.com/co/tablet/lg-LGV480-gpad">LG GPAD LGV 480</a></strong> que sortearemos diariamente.</p>
  </section>
  <!--PRIZE LEGEND-->
  <legend class="przBblMsg">
    <img src="imgs/prizePAD.png" align="left" width="">
      <p>¡Conoce el reto diario y podrás ganar una <strong>LG GPAD LGV480</strong>!</p>
      <a onclick="ga('send','event', 'Premios ','Click','/Botón-Participa ');" href="<?php echo base_url('site/engagement_challenge') ?>" class="btnCta">Participa</a>
  </legend>
</div><!--/.grayPtrnBg.container-offTp-->


<!-- PRIZE BLOCK SECTION -->
<div style="visibility:hidden;" class="sg-body sg-homeCont grayPtrnBg container-offBt">
  <section class="row">
    <div class="col-xs-12 col-md-12">
      <h1 class="titlBrdrLft">Premios</h1></div>
      <!--PRIZES FIGURES-->
      <article id="prizesHome" class="col-xs-12 col-md-12">

        <figure class="figPrizeHm" id="przHm1">
          <a onclick="ga('send','event', 'Premios ','Click','/Botón-Puesto1-Tv');" href="http://www.lg.com/co/televisores/lg-55LB650T-webos-3d-smart-tv" target="_blank" class="iconMoreInfo">
            <div class="icoPos"><span>1</span></div>
            <img src="imgs/prizeLG-TV.png" />
            <figcaption>
                <p><strong>TV LG 55LB650T CINEMA 3D</strong> SMART TV CON WEBOS y magic control</p><span class="icoMrInf">+</span>
            </figcaption>
          </a>
        </figure>

        <figure class="figPrizeHm" id="przHm2">
          <a onclick="ga('send','event', 'Premios ','Click','/Botón-Puesto2-Lavadora');" href="http://www.lg.com/co/lavasecadoras/lg-WFS1439EPD-carga-superior" target="_blank" class="iconMoreInfo">
            <div class="icoPos"><span>2</span></div>
            <img src="imgs/prizeLG-Lavadora.png" />
            <figcaption>
                <p><strong>Lavadora LG WFS1439EPD</strong><br/>6 MOTION DD, CARGA SUPERIOR, 31LBS/14KG, CALENTADOR DE AGUA/ VAPOR</p><span class="icoMrInf">+</span>
            </figcaption>
          </a>
        </figure>

        <figure class="figPrizeHm" id="przHm3">
          <a onclick="ga('send','event', 'Premios ','Click','/Botón-Puesto3-Nevera');" href="http://www.lg.com/co/neveras/lg-GT46SGP" target="_blank" class="iconMoreInfo">
            <div class="icoPos"><span>3</span></div>
            <img src="imgs/prizeLG-Nevera.png" />
            <figcaption>
                <p><strong>Nevera LG GT46SGP 457LTS</strong>, SMART INVERTER COMPRESSOR, HYGIENE FRESH+, ICEDOOR. ELIMINA BACTERIAS HASTA UN 99,999%</p><span class="icoMrInf">+</span>
            </figcaption>
          </a>
        </figure>

        <figure class="figPrizeHm" id="przHm4">
          <a onclick="ga('send','event', 'Premios ','Click','/Botón-Puesto4-Pc');" href="http://www.lg.com/co/computadores/lg-11T540-tablet-computador" target="_blank" class="iconMoreInfo">
            <div class="icoPos"><span>4</span></div>
            <img src="imgs/prizeLG-PC.png" />
            <figcaption>
                <p><strong>PC LG Tab book 11T540</strong><br/>WINDOWS 8.1,TAB-BOOK 2, FULL HD, MULTI TOUCH, ULTRA LIVIANO, PANTALLA IPS FULL HD</p><span class="icoMrInf">+</span>
            </figcaption>
          </a>
        </figure>

        <figure class="figPrizeHm" id="przHm5">
          <a onclick="ga('send','event', 'Premios ','Click','/Botón-Puesto5-Smartphone');" href="http://www.lg.com/co/products/wtb?modelName=D855" target="_blank" class="iconMoreInfo">
            <div class="icoPos"><span>5</span></div>
            <img src="imgs/prizeLG-Phone.png" />
            <figcaption>
                <p><strong>Smartphone LG G3</strong><br/>Quad-core, Pantalla IP, 4G</p><span class="icoMrInf">+</span>
            </figcaption>
          </a>
        </figure>

        <figure class="figPrizeHm" id="przHm6">
          <a onclick="ga('send','event', 'Premios ','Click','/Botón-Puesto6-Barra-Sonido');" href="http://www.lg.com/co/teatros-en-casa/lg-NB5540" target="_blank" class="iconMoreInfo">
            <div class="icoPos"><span>6</span></div>
            <img src="imgs/prizeLG-Teatro.png" />
            <figcaption>
                <p><strong>Barra de sonido LG HS7 LG</strong><br/>MUSICFLOW SMART HI-FI AUDIO MULTI-ROOM INALÁMBRICO. TRANSMISION DE AUDIO SOBRE RED WI-FI</p><span class="icoMrInf">+</span>
            </figcaption>
          </a>
        </figure>

        <div class="ovrfwd rght">
          <div class="bubbleCrcLG pnkBg">
            <span></span>
              <p>Cumple los retos, haz que tus amigos interactuen con tu post y podrás equipar a mamá con increibles premios LG.</p>
          </div>
        </div>

        <nav class="nvsBtnsCta text-right">
          <a onclick="ga('send','event', 'Premios ','Click','/Botón-Participa ');" href="<?php echo base_url('site/my_challenges') ?>" class="btnCta grayBtn"><span class="fa fa-facebook"> | </span>Partcipa</a>
        </nav>

      </article><!--/.prizesHome-->
<p class="warning-txt" id="homeWarningTxt" >Imágenes de referencia, las características y especificaciones están sujetos a cambios sin previo aviso.</p>
  </section>
</div>
<!--/.grayPtrnBg.container-offTp-->