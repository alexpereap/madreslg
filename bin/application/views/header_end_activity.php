<!DOCTYPE html>
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <base href="<?php echo base_url() ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Quiere a tu mamá en Facebook y equípala con lo mejor de LG">
    <meta name="keywords" content="mes madres, mamá, lg, electrodomesticos, regalos">
    <meta name="author" content="LG Colombia">

    <!-- Facebook open graph metatags -->
    <meta property="og:site_name" content="Equipa a mamá con LG"/>
    <meta property="og:title" content="Equipa a mamá con LG" />
    <meta property="og:url" content="<?php echo base_url(); ?>" />
    <meta property="og:description" content="Yo demuestro lo mucho que quiero a Mamá para equiparla con increíbles premios, #YoQuieroAMamáConLG." />
    <meta property="fb:app_id" content="664192710392558" />
    <meta property="og:image" content="<?php echo base_url('imgs/POST_share_small.jpg'); ?>" />

  <title>Equipa a mamá con LG</title>
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/styleguide.css">
  <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
  <!-- Style Guide Reset -->
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="css/lgmadres_override.css?v=<?php echo time() ?>">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Favicons -->
  <!--link rel="apple-touch-icon" href="/apple-touch-icon.png"-->
  <link rel="icon" href="<?php echo base_url();?>imgs/favicon.ico">
<link rel="stylesheet" href="vendor/jquery-ui-1.11.1.custom/smoothness/jquery-ui.css">

<link rel="stylesheet" href="<?php echo base_url();?>vendor/bootstrapvalidator-dist-0.5.1/dist/css/bootstrapValidator.css">

<!-- Add fancyBox -->
<link rel="stylesheet" href="vendor/fancybox2/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />


</head>
<!-- INIT LANDING -->
<body  data-target=".navbar-default" data-offset="60" class="lg-mothers <?php echo isset( $in_home ) ? 'fixBg' : 'toTop ' ?>" >

<!-- Facebook JS SDK -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.3&appId=664192710392558";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!--NAV MAINBAR FIXXED-->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="sg-head sg-container">
  <!-- ==================================================
    header: -->
    <header class="headLG container">
      <div class="row">
        <!--logoLg-->
        <div class="col-xs-12 col-sm-5 col-md-7">
          <a onclick="ga('send','event', 'Header ','Click','/Botón-Logo-LG');" href="#" class="logoHead">
            <img src="imgs/logo-LG-def.png" title="LG - Life´s Good"></a><!--/.logoLg-->
            <!--nav class="navHead collapse navbar-collapse cbp-spmenu cbp-spmenu-vertical" id="bs-example-navbar-collapse-1"-->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>
              
        </div>
        <!--nets navs-->
        <div class="col-xs-12 col-sm-5 col-md-5">
            <nav class="navSocialHead">
              <li><span class="titNets">Síguenos en:</span></li>
              <li><a onclick="ga('send','event', 'Redes Sociales ','Click','/Botón-Siguenos-Facebook ');" target="_BLANK" href="https://www.facebook.com/LGColombia?fref=ts" class="shdwInst"><span class="fa fa-facebook fa-2x"></span></a></li>
              <li><a onclick="ga('send','event', 'Redes Sociales ','Click','/Botón-Siguenos-Twitter');"   target="_BLANK" href="https://twitter.com/soylg" class="shdwInst"><span class="fa fa-twitter fa-2x"></span></a></li>
              <li><a onclick="ga('send','event', 'Redes Sociales ','Click','/Botón-Siguenos-Youtube ');"  target="_BLANK" href="https://www.youtube.com/user/LGCOLchannel" class="shdwInst"><span class="fa fa-youtube fa-2x"></span></a></li>
              <li><a onclick="ga('send','event', 'Redes Sociales ','Click','/Botón-Siguenos-Google+');"   target="_BLANK" href="https://plus.google.com/+LGCOLchannel/posts" class="shdwInst"><span class="fa fa-google-plus fa-2x"></span></a></li>
              <li><a onclick="ga('send','event', 'Redes Sociales ','Click','/Botón-Siguenos-LG-Blog');"   target="_BLANK" href="http://lgblog.lgecb.com/" class="shdwInst extBtn">LG Blog</a></li>
              <li><a onclick="ga('send','event', 'Redes Sociales ','Click','/Botón-Siguenos-LG-Musik');"  target="_BLANK" href="http://www.lg.com/co/microsite/music-flow/music-flow.git/html/index.html" class="extLink"><img src="imgs/logoLGMusic.png"></a></li>
            </nav>
        </div>
        <!--/#navSession-->
      </div>
    </header><!--/.row-->
    <!--/.headCmpgn-->
  </div><!--/.sg-head-->
</div><!--/.navbar-default.navbar-fixed-top-->
