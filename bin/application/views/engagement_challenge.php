<!--HEADER INTRO SECT-->
<div class="sg-body sg-headCont container">
  <header class="hdrHeadInt">
    <hgroup class="row hGrpTitHead maxWidth"><!--text-center-->
      <img src="imgs/momsShdwLogo.png">
      <h1 class="headTit">Reto Diario<p class="msgMom"> <strong>Quiere a tu mamá</strong> en <span>Facebook</span> y equípala con lo mejor de <strong>LG</strong></p></h1>
    </hgroup>
  </header>
</div><!--/.sg-headCont-->

<!-- BLOCK SECTION -->
<div class="sg-body sg-intCont leftSid container-fluid">
  <section class="row">
  <p class="text-center" >Cumple el reto diario, haz una captura de pantalla o toma una foto y ¡sube la imagen!</p>
    <div class="col-xs-12 col-md-6 col-sm-6">
      <article class="artclDarelG"><!--col-md-offset-3-->

      <?php if($this->session->flashdata('success') === FALSE): ?>

          <?php if( $engagement_challenge === false ): ?>
          <!--MESSAGE NO-CHALLENGE-->
          <h2 class="titDare">Ya participaste por el reto de hoy</h2>
          <p>Puedes volver mañana por el próximo reto.</p>
          <?php else: ?>
          <!--MESSAGE CHALLENGE-->
          <h2 class="titDare">Tu reto de hoy</h2>
          <p><?php echo $engagement_challenge->description_engage_challenge ?></p>
          <p id="engagementHint" >Recuerda que debes cumplir este reto en el fan page de LG Colombia, hacer una captura de pantalla o tomar una foto y subirla acá mismo.</p>
          <!--NAVS-->
          <?php endif; ?>
          <nav class="nvsBtnsCta">
            <a onclick="ga('send','Event', 'Reto-Díario ','Click','/Botón-Ir-al-Fanpage');" target="_BLANK" href="https://www.facebook.com/LGColombia?fref=ts" class="btnCta">Ir al fan Page</a>
            <?php if( $engagement_challenge ): ?>
              <a onclick="ga('send','Event', 'Reto-Díario ','Click','/Botón-Subir-Imagen-Reto ');" href="javascript:void(0);" id="uploadEngChallenge" class="btnCta">Subir imagen</a>

              <form id="engagementChallengeForm" action="<?php echo base_url('site/submit_engagement_challenge') ?>"  method="POST"  enctype="multipart/form-data"  >
              <input type="hidden" name="engagement_challenge_id" value="<?php echo $engagement_challenge->id ?>" >
              <input type="file" style="display:none;" name="engagementChallengeFile" id="engagementChallengeFile">
              </form>
              <a  style="display:none;" href="javascript:void(0);" id="submitEngageChallenge" class="btnCta">Envíar</a>
            <?php endif; ?>
          </nav>
        </nav><!--/.nvsBtnsCta-->

      <?php else: ?>
        <h2><?php echo $this->session->flashdata('success_title') ?></h2>
        <p><?php echo $this->session->flashdata('success') ?></p>
      <?php endif; ?>
      </article><!--/.artclDarelG-->
    </div>
    <div class="col-xs-12 col-md-6 col-sm-6">
      <!--PRODUCT IMAGE-->
      <figure class="figDareLG">
        <!--img src="imgs/retoImg01.jpg" /--> 
        <img src="imgs/LGGPAD.png" /> 
        <figcaption class="mrg-fix" >
          <p>Participas por una <strong>LG PAD 7</strong> <span class="ref">LGV480</span><a onclick="ga('send','Event', 'Reto-Díario ','Click','/Botón-Conoce-Más-Lg-Pad');" target="_BLANK" href="http://www.lg.com/co/tablet/lg-LGV480-gpad" class="btnCta">Conoce Más</a></p></figcaption>
      </figure><!--/.figDareLG-->
    </div>
  </section><!--/.row-->

  <!--PRIZE LEGEND-->
 <!-- <legend class="przBblMsg">
    <img src="imgs/prizePAD.png" align="left" width="">
      <p>¡Conoce el reto diario y podrás ganar una <strong>LG GPAD LGV480</strong>!</p>
      <a href="#" class="btnCta">Participa</a>
  </legend>
  -->
</div><!--/.sg-body.sg-homeIntro-->