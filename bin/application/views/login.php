<?php if( !isset($in_pre_register) ): ?>
<a href="<?php echo $login_url ?>">Conectar</a>
<?php endif; ?>

<?php if( isset($in_pre_register) ): ?>

<script>
    var user_friends = <?php echo json_encode( $user_friends, JSON_PRETTY_PRINT ) ?>
</script>

    <form method="POST" action="<?php echo base_url('site/handleUserReg') ?>" >

        <h1>usuario</h1>

        <input type="hidden" name="user_fb_id" value="<?php echo $user_data['facebook_id'] ?>" >
        <input type="text" name="user_firstname" value="<?php echo $user_data['first_name'] ?>"  >
        <input type="text" name="user_lastname" value="<?php echo $user_data['last_name'] ?>"  >

        <select name="user_birth_day" >
            <?php for( $i = 1; $i <= 30; $i++ ):  $val = $i < 10 ? '0' . $i : $i; ?>
                <option <?php if( explode('-',$user_data['birth_date'])[2] == $i ) echo 'selected' ?> value="<?php echo $val ?>"><?php echo $val ?></option>
            <?php endfor; ?>
        </select>
        <select name="user_birth_month" >
            <?php for( $i = 1; $i <= 12; $i++ ): $val = $i < 10 ? '0' . $i : $i; ?>
                <option  <option <?php if( explode('-',$user_data['birth_date'])[1] == $i ) echo 'selected' ?> value="<?php echo $val ?>"><?php echo $val ?></option>
            <?php endfor; ?>
        </select>
        <select name="user_birth_year" >
            <?php for( $i = 1900; $i <= 2000; $i++ ): ?>
                <option  <option <?php if( explode('-',$user_data['birth_date'])[0] == $i ) echo 'selected' ?> value="<?php echo $i ?>"><?php echo $i ?></option>
            <?php endfor; ?>
        </select>

        <input type="text" name="user_email" value="<?php echo $user_data['email'] ?>"  >
        <input type="text" name="user_document" placeholder="user_document" >
        <input type="text" name="user_address" placeholder="user_address" >
        <input type="text" name="user_city" value="<?php echo $user_data['city'] ?>" >
        <input type="text" name="user_cellphone" placeholder="user_cellphone" >

        <h1>mama</h1>

        <input class="mom-field" type="text" id="search" placeholder="buscar" >
        <input class="mom-field" type="hidden" name="mom_fb_id" id="momFbId" >
        <input class="mom-field" type="hidden" name="mom_picture" id="momPicture" >

        <p>tu mama es: <b><span id="momNameTxt"></span></b> </p>

        <input class="mom-field" type="text" name="mom_name" id="momName" placeholder="mom_name" >
        <input class="mom-field" type="text" name="mom_lastname" id="momLastname" placeholder="mom_lastname" >
        <input class="mom-field" type="text" name="mom_email" placeholder="mom_email" >
        <input class="mom-field" type="text" name="mom_document" placeholder="mom_document" >
        <input class="mom-field" type="text" name="mom_address" placeholder="mom_address" >

        <input type="submit" value="participar">
    </form>
<?php endif; ?>