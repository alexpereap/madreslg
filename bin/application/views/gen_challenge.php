<!-- Facebook preview modal -->
<div id="fbPostPreview" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Vista Previa<a class="anchorjs-link" href="#mySmallModalLabel"><span class="anchorjs-icon"></span></a></h4>
        </div>
        <div class="modal-body">
          <p id="fbPreviewDescription" ></p>

          <div class="col-md-6"><img width="100%" alt="" src="" id="fbPreviewImage"></div>
          <div class="col-md-6"><textarea name="" id="fbPreviewMessage" rows="7" style="width:100%" ></textarea></div>
          <a id="postChallengeOnFb" class="modal-post-cta pull-right">Publicar en Facebook</a>
          <a id="cancelPostChallegeOnFb" data-dismiss="modal" aria-label="Close" class="modal-post-cta pull-right ">Cancelar</a>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>

<!--/.sg-intCont.leftSid-->

<?php if( $current_challenge ): ?>
<!-- BLOCK SECTION: Challenge Details -->
<div class="sg-body sg-intCont leftSid container-fluid">
  <section class="row" >
    <div class="col-xs-12 col-md-6 col-sm-6" >
        <article class="artclDarelG" ><!--col-md-offset-3-->
            <!--MESSAGE NO-CHALLENGE-->
            <h2 class="titDare crrntChllng">Tu reto es:</h2>
            <p id="currentChallengeDescription" class="crrntChllngTxt">“<?php echo $current_challenge!= false ? $current_challenge->challenge_description : null; ?>”</p>
            <p id="engagementHint" >Recuerda que debes cumplir el reto, hacer una captura de pantalla o tomar una foto y subirla acá mismo.</p>
            <!--NAVS-->
            <nav class="nvsBtnsCta text-left" >
              <a onclick="ga('send','Event', 'Categoria-<?php echo $current_challenge->category_description; ?>','Click','/Botón-Subir-Imagen-Reto');" href="javascript:void(0);" id="uploadImg" class="btnCta">Subir imagen</a>
              <form id="sendChallengeForm" method="POST" action="<?php echo base_url('site/submit_challenge') ?>"  enctype="multipart/form-data"  >
                  <input style="display:none;" type="file" name="challengeFile" id="challengeFile">
                  <textarea name="challengeText" id="challengeText" style="display:none;" ></textarea>
              </form>
            </nav>
          </nav><!--/.nvsBtnsCta-->
        </article><!--/.artclDarelG-->
    </div>
    <div class="col-xs-12 col-md-5 col-sm-6">
      <!--PRODUCT IMAGE-->
      <figure class="figDareLG crrntDareFig">
        <img src="imgs/<?php echo $prize_salepoints[0]->img_prize ?>" />
        <figcaption>
          <h5><?php echo $current_challenge!= false ? $current_challenge->prize_title : null; ?></h5>
          <span class="ref"><?php echo $current_challenge->prize_ref ?></span>
          <p><?php echo $current_challenge->prize_description ?></p>
          <!--SPECS-->
          <!-- <ul class="spcsPrdctLG">
            <li>NUEVA PLATAFORMA SMART WEBOS</li>
            <li>CINEMA 3D</li>
            <li>PANEL IPS</li>
            <li>CINEMA 3D</li>
            <li>MAGIC REMOTE</li>
          </ul> -->
          <!--RETAILERS CIONS LIST-->
          <ul class="retailsIcons">
            <li class="lnkWhrPrchs"><a>Dónde Comprar</a></li>
              <?php foreach( $prize_salepoints as $s ): ?>
              <li><a onclick="ga('send','Event', 'Catégoria-<?php $current_challenge->category_description ?>','Click','/Botón-Comprar-<?php echo $s->description_sale_point ?>');" target="_BLANK" href="<?php echo $s->url_sale_point ?>"><img src="imgs/<?php echo $s->img_sale_point ?>" title="<?php echo $s->description_sale_point ?>" /></a></li>
              <?php endforeach; ?>
          </ul>
        </figcaption>
        <p class="warning-txt" >Imágenes de referencia, las características y especificaciones están sujetos a cambios sin previo aviso.</p>
      </figure><!--/.figDareLG.crrntDareFig-->
    </div>
  </section><!--/.row-->
</div><!--/.sg-intCont.leftSid-->

<?php endif; ?>