<!--HEADER INTRO SECT-->
<div class="sg-body sg-headCont container">
  <header class="hdrHeadInt">
    <hgroup class="row hGrpTitHead text-center">
      <img src="imgs/momsShdwLogo.png">
      <h1 class="headTit">Regístrate</h1>
    </hgroup>
  </header>
</div><!--/.sg-headCont-->

<!-- BLOCK SECTION -->
<div class="sg-body sg-intCont container">

  <?php if( !isset($in_pre_register) ): ?>
  <section class="row">
    <p class="minPWidth text-center">Ingresa con Facebook<a onclick="ga('send','event', 'Regístrate','Click','/Botón-Facebook-Conectar');" href="<?php echo $login_url ?>" class="btnCta btnConnctFbk"><span class="fa fa-facebook"> | </span>conectar</a></p>
  </section><!--/.row-->
  <?php else: ?>

    <?php if( isset($user_friends) ): ?>
    <script>
      var user_friends = <?php echo json_encode( $user_friends, JSON_PRETTY_PRINT ) ?>
    </script>
    <?php endif; ?>
  <section class="row">
    <!--FORM-->
      <form method="POST" id="registerForm" action="<?php echo base_url('site/handleUserReg') ?>" class="form frmLGDef">
      <input type="hidden" name="user_fb_id" value="<?php echo $user_data['facebook_id'] ?>" >
      <div id="userDataWrapper" class="col-xs-12 col-md-6 col-sm-6">
        <h2 class="subTit iconUsrLG prsnMnLG">Tus datos</h2>
          <fieldset class="form-group">
            <!--label class="control-label" for=""></label-->
            <input type="text" name="user_firstname" value="<?php echo $user_data['first_name'] ?>"  class="form-control" id="" placeholder="Nombres">
          </fieldset>
          <fieldset class="form-group">
            <input type="text" name="user_lastname" value="<?php echo $user_data['last_name'] ?>" class="form-control" id="" placeholder="Apellidos">
          </fieldset>
          <fieldset class="form-group">
            <label class="control-label" for="">Fecha de nacimiento</label>
            <br/>
            <label class="drpCbLG">
                <select name="user_birth_day" class="dateSlct">
                <?php for( $i = 1; $i <= 31; $i++ ):  $val = $i < 10 ? '0' . $i : $i; ?>
                    <option <?php if( explode('-',$user_data['birth_date'])[2] == $i ) echo 'selected' ?> value="<?php echo $val ?>"><?php echo $val ?></option>
                <?php endfor; ?>
                </select>
            </label>
            <label class="drpCbLG">
                <select name="user_birth_month" class="dateSlct">
                  <?php for( $i = 1; $i <= 12; $i++ ): $val = $i < 10 ? '0' . $i : $i; ?>
                    <option  <option <?php if( explode('-',$user_data['birth_date'])[1] == $i ) echo 'selected' ?> value="<?php echo $val ?>"><?php echo $val ?></option>
                  <?php endfor; ?>
                </select>
            </label>
            <label class="drpCbLG">
                <select name="user_birth_year" class="dateSlct">
                  <?php for( $i = 1900; $i <= 2000; $i++ ): ?>
                    <option  <option <?php if( explode('-',$user_data['birth_date'])[0] == $i ) echo 'selected' ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                  <?php endfor; ?>
                </select>
            </label>
          </fieldset>
          <fieldset class="form-group">
            <input type="email" name="user_email" value="<?php echo $user_data['email'] ?>" class="form-control" id="" placeholder="Escribe tu correo electrónico">
          </fieldset>
          <fieldset class="form-group">
            <input type="text" name="user_document" class="form-control" id="" placeholder="Cédula">
          </fieldset>
          <fieldset class="form-group">
            <input type="text" name="user_address" class="form-control" id="" placeholder="Dirección">
          </fieldset>
          <fieldset class="form-group">
            <input type="text" name="user_city" value="<?php echo $user_data['city'] ?>" class="form-control" id="" placeholder="Ciudad">
          </fieldset>
          <fieldset class="form-group">
            <input type="text" class="form-control" name="user_cellphone" id="" placeholder="Número Celular">
          </fieldset><br/>
      </div><!--/.col-md-6-->

      <div class="col-xs-12 col-md-6 col-sm-6">
        <br/>
          <?php if( true ): ?>
          <fieldset class="form-group">
            <p>Si tu mamá está entre tus amigos de Facebook escribe su nombre:</p>
            <input onclick="ga('send','event', 'Regístrate','Click','/Botón-Facebook-Busca-a-Mamá');" type="text" class="mom-field form-control fbkSearchInput bgBlank" id="search" >
            <input class="mom-field" type="hidden" name="mom_fb_id" id="momFbId" >
            <input class="mom-field" type="hidden" name="mom_picture" id="momPicture" >
            <p id="momByFbWrapper" >Tu mamá es<span id="momNameTxt" class="titPrpl"><!-- JS FILLED --></span><br/></p>
            <p id="momNoFbWrapper" >Si no está en Facebook completa sus datos:</p>
          </fieldset>
          <?php endif; ?>
        <h2 class="subTit iconUsrLG prsnMomLG">Datos de tu mama</h2>
          <fieldset class="form-group">
            <!--label class="control-label" for=""></label-->
            <input type="text" class="form-control mom-field" name="mom_name" id="momName" placeholder="Nombres">
          </fieldset>
          <fieldset class="form-group">
            <input type="text" class="form-control mom-field" name="mom_lastname" id="momLastname" placeholder="Apellidos">
          </fieldset>
          <fieldset class="form-group">
            <input type="email" class="form-control mom-field" name="mom_email"  placeholder="Escribe su correo electrónico">
          </fieldset>
          <fieldset class="form-group">
            <input type="text" class="form-control mom-field" name="mom_document" placeholder="Cédula">
          </fieldset>
          <fieldset class="form-group">
            <input type="text" class="form-control mom-field" name="mom_cellphone" placeholder="Número Celular">
          </fieldset><br/>
      </div><!--/.col-md-6-->
      <!--LEGALS-->
          <div class="radio text-center">
             <input previousValue='false' id="radio-1" type="radio" name="terms" value="1" >
             <label for="radio-1">Acepto los <a href="<?php echo base_url('terminos_y_condiciones.pdf') ?>" target="_blank">términos y condiciones </a></label>
          </div>
          <div class="radio text-center">
             <input previousValue='false' id="radio-2" type="radio" name="suscribe" value="1">
             <label for="radio-2">Acepto recibir información de productos y promociones.</label>
          </div>
          <br/>
          <input onclick="ga('send','event', 'Regístrate','Click','/Botón-Participa ');" type="submit" class="btn btn-default sbmtCta" value="Participar" />
      </form><!--frmDefLGComp-->

  </section><!--/.row-->
  <?php endif; ?>
</div><!--/.sg-body.sg-homeIntro-->