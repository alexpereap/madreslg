<!--HEADER INTRO SECT-->
<div class="sg-body sg-headCont container">
  <header class="hdrHeadInt">
    <hgroup class="row hGrpTitHead maxWidth"><!--text-center-->
      <img src="imgs/momsShdwLogo.png">
      <h1 class="headTit">Premios<p class="msgMom"> <strong>Quiere a tu mamá</strong> en <span>Facebook</span> y equípala con lo mejor de <strong>LG</strong></p></h1>
    </hgroup>
  </header>
</div><!--/.sg-headCont-->


<!-- BLOCK SECTION: Intro Prizes -->
<div class="sg-body sg-intCont container-fluid">

  <!--PRIZE LEGEND-->
  <!--<legend class="przBblMsg" style="top:12%;">
    <img src="imgs/prizePAD.png" align="left" width="">
      <p>¡Conoce el reto diario y podrás ganar una <strong>LG GPAD LGV480</strong>!</p>
      <a href="<?php echo base_url('site/engagement_challenge') ?>" class="btnCta">Participa</a>
  </legend>-->

  <div id="carousel-prizes" class="carousel slide carousel-fade sldPrizesLG" data-ride="carousel" data-interval="6000">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li onclick="ga('send','event', 'Premios ','Click','/Botón-Puesto1-Tv');" data-target="#carousel-prizes" data-slide-to="0" class="active">1</li>
      <li onclick="ga('send','event', 'Premios ','Click','/Botón-Puesto2-Lavadora');" data-target="#carousel-prizes" data-slide-to="1">2</li>
      <li onclick="ga('send','event', 'Premios ','Click','/Botón-Conoce-Reto-Puesto3-Nevera');" data-target="#carousel-prizes" data-slide-to="2">3</li>
      <li onclick="ga('send','event', 'Premios ','Click','/Botón-Conoce-Reto-Puesto4-Pc');" data-target="#carousel-prizes" data-slide-to="3">4</li>
      <li onclick="ga('send','event', 'Premios ','Click','/Botón-Conoce-Reto-Puesto5-Smartphone');" data-target="#carousel-prizes" data-slide-to="4">5</li>
      <li onclick="ga('send','event', 'Premios ','Click','/Botón-Conoce-Reto-Puesto6-Barra-Sonido');" data-target="#carousel-prizes" data-slide-to="5">6</li>
    </ol>
    <section class="row">
        <br/>
        <p class="msgIntro text-center"><span>Los primeros 6 en el ranking</span> podrán ganar los siguientes premios:</p>
        <article class="col-xs-12 col-md-12 col-sm-12">
        <br/>
        <div class="icoPos"><span>1</span></div>
          <!-- Wrapper for slides -->
          <div class="carousel-inner">
          <?php foreach( $prizes as $key => $p ): ?>
            <div <?php if( $key == 0 ): ?> class="item active" <?php else: ?> class="item" <?php endif; ?> >
              <figure class="figDareLG crrntDareFig">
                <img src="imgs/<?php echo $p->img_prize ?>" alt="<?php echo $p->description_prize ?>">
                <figcaption class="carousel-caption">
                  <h5><?php echo $p->title_prize ?></h5>
                    <span class="ref"><?php echo $p->ref  ?></span>
                    <!--SPECS-->
                    <ul class="spcsPrdctLG">
                      <li><?php echo $p->description_prize ?></li>
                    </ul><!--/.spcsPrdctLG-->
                    <nav class="nvsBtnsCta text-left">
                      <a onclick="<?php echo $category_prizes_ga[$key]; ?>" href="<?php echo base_url('site/my_challenges') ?>" class="btnCta">Conoce tu reto</a>
                    </nav>
                    <!--RETAILERS CIONS LIST-->
                    <ul class="retailsIcons">
                      <li class="lnkWhrPrchs"><a>Dónde Comprar</a></li>
                        <?php foreach( $p->salepoints as $sp ): ?>
                        <li><a onclick="ga('send','event', '<?php echo $category_prizes_idx[$key] ?>','Click','/Botón-Comprar-<?php echo $sp->description_sale_point ?>');" target="_BLANK" href="<?php echo $sp->url_sale_point ?>"><img src="imgs/<?php echo $sp->img_sale_point ?>" title="<?php echo $sp->description_sale_point  ?>" /></a></li>
                        <?php endforeach; ?>
                    </ul><!--/.retailsIcons-->
                </figcaption><!--/.carousel-caption-->
              </figure><!--/.figDareLG.crrntDareFig-->
            </div>
            <?php endforeach; ?>
            <!--/.item-->
            <!--/.item-->
            <!--/.item-->
            <!--/.item-->
            <!--/.item-->
            <!--/.item-->
          </div><!--/.carousel-inner-->
          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-prizes" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
          </a>
          <a class="right carousel-control" href="#carousel-prizes" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
        </article><!--/.intRankArtcl-->
        <br/>
    <p class="warning-txt" id="prizesWarningTxt" >Imágenes de referencia, las características y especificaciones están sujetos a cambios sin previo aviso.</p>
    </section><!--/.row-->
  </div> <!-- Carousel -->
</div><!--/.sg-intCont-->