<!--HEADER INTRO SECT-->
<div class="sg-body sg-headCont container">
  <header class="hdrHeadInt">
    <hgroup class="row hGrpTitHead maxWidth"><!--text-center-->
      <img src="imgs/momsShdwLogo.png">
      <h1 class="headTit">Mecánica<p class="msgMom"> <strong>Quiere a tu mamá</strong> en <span>Facebook</span> y equípala con lo mejor de <strong>LG</strong></p></h1>
    </hgroup>
  </header>
</div><!--/.sg-headCont-->


<!-- BLOCK SECTION: Mechanics Int -->
  

<div class="sg-body sg-intCont container-fluid">
    <p class="text-center">Tienes dos formas de participar y equipar a mamá en su mes con LG:</p>
    <br/>
  <section class="row boxGrayShdw">
    <br/><br/>
    <article class="col-xs-12 col-md-5 col-sm-12">
      <img src="imgs/mechncsGameInt.png">
    </article>
    <article class="col-xs-12 col-md-7 col-sm-12 toMidVrtcll">
      <div class="ntLftBrdrGray"><!--lftBrdrGray-->
      <h2 class="subTit"><span>1.</span> ¿Quieres equipar a mamá con lo mejor de LG? </h2>
        <ol class="listOrdrd">
          <li><span>1.</span> Ingresa tus datos personales junto con los datos de tu mamá.</li>
          <li><span>2.</span> Te daremos retos en los cuales tendrás que demostrar tu cariño por mamá.</li>
          <li><span>3.</span> Toma una foto cumpliendo el reto y publícalo a través de nuestra aplicación.</li>
          <li><span>4.</span> Los 6 primeros participantes del Ranking recibiran <a href="<?php echo base_url('site/prizes') ?>">increíbles premios LG.</a></li>
          <li><span>5.</span> Tu posición en el ranking se verá incrementada por el número de shares e interacciones que reciba el reto publicado en tu wall.</li>
        </ol>
      </div>
          <nav class="nvsBtnsCta text-left">
            <a onclick="ga('send','event', 'Mecánica','Click','/Botón-Participa ');" href="<?php echo base_url('site/my_challenges') ?>" class="btnCta"><?php echo $this->session->userdata('user_public') ? 'Mis Retos' : 'Participa';  ?></a>
          </nav>
      <br/>
    </article>
  </section><!--/.row-->
  <!--PRIZE LEGEND-->
  <!--legend class="przBblMsg">
    <img src="imgs/prizePAD.png" align="left" width="">
      <p>¡Conoce el reto diario y podrás ganar una <strong>LG GPAD LGV480</strong>!</p>
      <a href="#" class="btnCta">Participa</a>
  </legend--> 
  <br/><br/>
  <section class="row">
    <br/><br/>
    <article class="col-xs-12 col-md-5 col-sm-12">
      <img src="imgs/prizePAD-2.png">
    </article>
    <article class="col-xs-12 col-md-7 col-sm-12 toMidVrtcll">
      <div class="ntLftBrdrGray">
      <h2 class="subTit"><span>2.</span> También puedes equiparla con lo mejor diariamente. </h2>
        <ol class="listOrdrd">
          <li><span>1.</span> Ingresa tus datos personales junto con los datos de tu mamá..</li>
          <li><span>2.</span> Cumple el reto diario..</li>
          <li><span>3.</span> Toma una foto cumpliendo el reto y súbelo a través de nuestra aplicación.</li>
          <li><span>4.</span> Entregaremos una <strong>LG GPAD LGV480</strong> todos los días.</li>
        </ol>
      </div>
          <nav class="nvsBtnsCta text-left">
            <a onclick="ga('send','event', 'Mecánica','Click','/Botón-Conoce-Reto-Diario-Participa');" href="<?php echo base_url('site/engagement_challenge') ?>" class="btnCta">¡Conoce el reto diario!</a>
          </nav>
      <p class="text-left termsMsg">*Ver detalles de la mecánica en <a target="_BLANK" href="<?php echo base_url('terminos_y_condiciones.pdf') ?>">términos y condiciones</a>.</p>
    </article>
  </section><!--/.row-->

</div>

<!--/.sg-intCont-->