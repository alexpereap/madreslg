<input type="hidden" id="momName" value="<?php echo trim($user_mom->first_name . ' ' . $user_mom->last_name); ?>" >

<!--HEADER INTRO SECT-->
<div class="sg-body sg-headCont container">
  <header class="hdrHeadInt">
    <hgroup class="row hGrpTitHead maxWidth"><!--text-center-->
      <img src="imgs/momsShdwLogo.png">
      <h1 class="headTit">Mis Retos<p class="msgMom"> <strong>Quiere a tu mamá</strong> en <span>Facebook</span> y equípala con lo mejor de <strong>LG</strong></p></h1>
    </hgroup>
  </header>
</div><!--/.sg-headCont-->

<!-- BLOCK SECTION: Intro Profile -->
<div class="sg-body sg-intCont container-fluid">
  <section class="row">
      <article class="col-xs-12 col-md-12 col-sm-12 infPrflUsrLG text-center">
          <!--p class="text-center">Ingresa tu datos personales en el formulario de registro o ingresa con</p-->
            <figure class="userProfileInf">
              <div class="userImage" ><img src="http://graph.facebook.com/<?php echo $this->session->userdata('user_public')->facebook_id ?>/picture?width=100&height=100" /></div>
                <figcaption><h4 class="userTitle"> <?php echo $this->session->userdata('user_public')->first_name . ' ' . $this->session->userdata('user_public')->last_name;  ?> </h4>
                  <div class="userPosition">
                    <span>Posición:</span>
                    <?php if( intval($user_rank) > 20 ){
                        $user_rank = '20+';
                    }

                    if( intval( $user_rank ) == 0 ){
                        $user_rank = '20+';
                    }
                    ?>
                    <strong><?php echo $user_rank; ?></strong></div>
                </figcaption>
            </figure>
      </article><!--infPrflUsrLG-->
      <br/><br/>
      <article class="col-xs-12 col-md-12 col-sm-10 col-md-offset-0 col-sm-offset-1 col-xs-offset-0">
          <p class="msgIntro text-center">Cumple los retos y sube tu posición en el ranking.</p>
          <?php if( $current_challenge === false && $available_challenge ): ?>
          <div id="genChallengeBtnWrapper" >
            <a onclick="ga('send','event', 'Mis-Retos','Click','/Botón-Generar-Reto');" href="javascript:void(0);" id="generateChallenge" class="btnCta">Generar Reto</a>
          </div>
          <?php endif; ?>
      </article>
  </section><!--/.row-->
  <br/><br/>
  <!--PRIZE LEGEND-->
  <!--<legend class="przBblMsg">
    <img src="imgs/prizePAD.png" align="left" width="">
      <p>¡Conoce el reto diario y podrás ganar una <strong>LG GPAD LGV480</strong>!</p>
      <a href="#" class="btnCta">Participa</a>
  </legend>-->

</div><!--/.sg-body.sg-homeIntro-->


<?php if( $this->session->flashdata('post_result') ): ?>
<div class="sg-body sg-intCont container-fluid">
<section class="row">
    <h2 class="text-center" ><?php echo $this->session->flashdata('post_result') ?></h2>
</section>
</div>
<?php endif; ?>

<?php if( !$available_challenge ): ?>
<div class="sg-body sg-intCont container-fluid">
<section class="row">
    <h2 class="text-center" >Ya participaste por el reto de hoy.
<br/>Puedes volver mañana por el próximo reto.</h2>
</section>
</div>
<?php endif; ?>




<?php if( $available_challenge ): ?>
<!-- BLOCK SECTION: Generate Challenge -->
<input type="text" style="visibility:hidden;" id="retoLg">
<div id="genChallengeWrapper">
<?php $this->view('gen_challenge'); ?>
</div>

<?php endif; ?>


<?php if( count($completed_challenges) > 0 ): ?>
<!-- BLOCK SECTION: Challenge Lists  -->
<div class="sg-body sg-intCont bgWhtTrnsp container-fluid">
  <section class="row">
    <div class="col-xs-12 col-md-10 col-sm-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-0">
      <h3 class="subTtlIntD">Retos cumplidos</h3>
      <ul class="chhlngLst">
        <?php foreach( $completed_challenges as $c ): ?>
        <li class="icoChllng">
          <a href="<?php echo base_url('uploads/' . $c->challenge_photo ) ?>" class="completed-challenge-img btnCta fltRgt">Ver imagen</a>
          <?php echo $c->challenge_description ?>
        </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </section>
</div>
<?php endif; ?>