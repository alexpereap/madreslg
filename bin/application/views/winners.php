<!-- BLOCK SECTION: Intro Winners -->
<div class="sg-body sg-intCont fullwdth container-fluid">
  <section class="row">

    <br/><p class="msgIntro text-center"><span>¡Estos son los felices ganadores</span> que demostraron lo mucho que quieren a su mamá!</p>
    <!--INIT INFO WINNERS-->
    <div class="col-xs-12 col-md-12 col-sm-12">
      <article class="artclFllPtrnlG intRankArtcl">
        <!--WINNERS LIST-->
        <ul class="winnersList">
          <li class="titleWinners">
            <h5>Jugador</h5>
            <h5>Mamá</h5>
            <h5>Premio</h5>
          </li><!--/.infoWinners-->
          <li class="infoWinners">
            <h5 class="childInfo"><img src="http://graph.facebook.com/1586705251612120/picture?width=100&height=100" />
              <span>Andrea Gonzalez Iñiguez</span></h5>
            <h5 class="momInfo"><img src="imgs/winnerMomImg.png" style="visibility:hidden;" />
              <span> Rossel Gonzalez Iñiguez</span></h5>
            <h5 class="prizeInfo">Televisor LG Ref:  55LB650T</h5>
          </li><!--/.infoWinners-->
          <li class="infoWinners">
            <h5 class="childInfo"><img src="http://graph.facebook.com/10152828785972286/picture?width=100&height=100" />
              <span>Geraldin Torres</span></h5>
            <h5 class="momInfo"><img src="imgs/winnerMomImg.png" style="visibility:hidden;" />
              <span> Elizabeth Vergara Olivera</span></h5>
            <h5 class="prizeInfo">Lavadora LG Ref: WFS1439EPD</h5>
          </li><!--/.infoWinners-->
          <li class="infoWinners">
            <h5 class="childInfo"><img src="http://graph.facebook.com/1578079062465055/picture?width=100&height=100" />
              <span>Diana Patricia Avila Martin</span></h5>
            <h5 class="momInfo"><img src="imgs/winnerMomImg.png" style="visibility:hidden;" />
              <span> Lina Maria Martin Urrego</span></h5>
            <h5 class="prizeInfo">Nevera LG Ref: GT46SGP</h5>
          </li><!--/.infoWinners-->
          <li class="infoWinners">
            <h5 class="childInfo"><img src="http://graph.facebook.com/10206561752263987/picture?width=100&height=100" />
              <span>Andrés Felipe León Snachez</span></h5>
            <h5 class="momInfo"><img src="imgs/winnerMomImg.png" style="visibility:hidden;" />
              <span> Luz Marina Sanchez Bermudez</span></h5>
            <h5 class="prizeInfo">PC LG Ref: Tab book 11T540</h5>
          </li><!--/.infoWinners-->
          <li class="infoWinners">
            <h5 class="childInfo"><img src="http://graph.facebook.com/981552951868877/picture?width=100&height=100" />
              <span>Karla Restrepo</span></h5>
            <h5 class="momInfo"><img src="imgs/winnerMomImg.png" style="visibility:hidden;" />
              <span> Yasmin Acevedo</span></h5>
            <h5 class="prizeInfo">Celular LG Ref G3 </h5>
          </li><!--/.infoWinners-->
          <li class="infoWinners">
            <h5 class="childInfo"><img src="http://graph.facebook.com/10204853391629234/picture?width=100&height=100" />
              <span>Sergio De Leon Cuao</span></h5>
            <h5 class="momInfo"><img src="imgs/winnerMomImg.png" style="visibility:hidden;" />
              <span> Maria Cuao Arias</span></h5>
            <h5 class="prizeInfo">Barra de sonido LG Ref LAS750 </h5>
          </li><!--/.infoWinners-->
        </ul><!--/.winnersList-->
      </article><!--/.intRankArtcl-->
    </div>

    <!--INIT INFO WINNERS-->
     <br/><p class="msgIntro text-center"><span>¡Estos son los felices ganadores</span> de una LG GPAD 480, que cumplieron el <a href="<?php echo site_url('site/engagement_challenge') ?>">Reto Diario!</a></p>
    <div class="col-xs-12 col-md-12 col-sm-12">
      <article class="artclFllPtrnlG intRankArtcl">
        <!--WINNERS LIST SCROLLED FIX HEAD-->
        <div id="scrlldLstWinnrs">
          <ul class="winnersList">
            <li class="titleWinners">
                <h5>Jugador</h5>
                <h5>Fecha</h5>
            </li><!--/.infoWinners-->
            <?php foreach( $winners_engagement as $w ): ?>
            <li class="infoWinners">
              <h5 class="childInfo"><img src="<?php echo $w->profile_picture ?>" />
                <span><?php echo $w->first_name . ' ' . $w->last_name ?></span></h5>
              <!--<h5 class="dateInfo">Martes 21 de Abril de 2015</h5>-->
              <h5 class="dateInfo"><?php echo utf8_encode (strftime( '%A %e de %B de %Y', strtotime( $w->created_at ) )) ?> </h5>
            </li><!--/.infoWinners-->
            <?php endforeach; ?>
          </ul><!--/.winnersList-->
        </div>
      </article>
    </div>
  </section><!--/.row-->
</div><!--/.sg-intCont-->