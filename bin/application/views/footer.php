<!--FOOTER MOM´S-->
<footer class="footLgMothers">
  <div class="sg-foot sg-container container">
      <hgroup class="messageMomLG">
        <img src="imgs/momsAlphaLogo.png" class="" />
        <h3><strong>Quiere a tu mamá</strong> en <span>Facebook</span> y equípala con lo mejor de <strong>LG</strong></h3>
        <nav class="navFoot">
              <li><a onclick="ga('send','event', 'Footer','Click','/Botón-Inicio');" href="<?php echo base_url() ?>" class="<?php if( isset($in_home) ) echo 'active' ?>">Inicio</a></li>
              <li> | </li>
              <li><a onclick="ga('send','event', 'Footer','Click','/Botón-Mecánica');" class="<?php if( isset($in_mechanical) ) echo 'active' ?>" href="<?php echo base_url('site/mechanical') ?>">Mécanica</a></li>
              <li> | </li>
              <li><a onclick="ga('send','event', 'Footer','Click','/Botón-Premios');" class="<?php if( isset($in_prizes) ) echo 'active' ?>" href="<?php echo base_url('site/prizes') ?>">Premios</a></li>
              <li> | </li>
              <li><a onclick="ga('send','event', 'Footer','Click','/Botón-Participa ');" class="<?php if( isset($in_my_challenges) ) echo 'active' ?>" href="<?php echo base_url('site/my_challenges') ?>">Participar</a></li>
              <li> | </li>
              <li><a  class="<?php if( isset($in_ranking) ) echo 'active' ?>" href="<?php echo base_url('site/ranking') ?>">Ranking</a></li>
              <li> | </li>
              <li><a  target="_BLANK" href="<?php echo base_url('terminos_y_condiciones.pdf') ?>" target="_blank">Términos y condiciones</a></li>
            </nav><!--/.navFoot-->
      </hgroup>
      <p class="copy"><!-- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse et sapien tempor, ultricies sem et, sagittis ex. Fusce a egestas est, eu condimentum mi. --></p>
  </div><!--/.sg-foot.sg-container-->
</footer><!--/.footLgMothers-->

<!--SCRIPTS-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <!--script src="js/bootstrap.min.js"></script-->
  <script src="js/scrollspy.js"></script>
  <script src="js/dropdown.js"></script>
  <script src="js/collapse.js"></script>
  <script src="js/sg-plugins.js"></script>
  <script src="js/sg-scripts.js"></script>

  <script src="vendor/jquery-ui-1.11.1.custom/jquery-ui.min.js" ></script>
  <script src="<?php echo base_url();?>vendor/bootstrapvalidator-dist-0.5.1/dist/js/bootstrapValidator.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url();?>vendor/bootstrapvalidator-dist-0.5.1/dist/js/language/es_ES.js"></script>

<script type="text/javascript" src="vendor/fancybox2/source/jquery.fancybox.pack.js?v=2.1.5"></script>

<script src="js/bootstrap.min.js"></script>
  <script src="js/site.js?v=<?php echo time() ?>" ></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-62216423-1', 'auto');
  ga('send', 'pageview');


</script>

</body>
</html>