<?php

Class Main extends CI_Model{


    public function getUser( $constraints ){

        $q = $this->db->get_where('tbl_user', $constraints );
        return count( $q->row() ) > 0 ? $q->row() : false;
    }

    public function saveUser( $data ){
        $this->db->insert('tbl_user', $data);
        return $this->db->insert_id();
    }

    public function saveUserMom( $data ){

        $this->db->insert('tbl_user_mom', $data);
        return $this->db->insert_id();
    }

    public function getAvailableChallenges($user_id)
    {
        $query = $this->db->get_where('vw_user_challenge_info', array('user_id' => $user_id, 'available_challenge >' => 0 ));
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

    public function getUserLastChallengeCompleted($user_id)
    {
        $query = $this->db->query('SELECT b.fk_user_id as user_id,
            c.id as challenge_id,
            c.fk_category_challenge_id
            from tbl_challenge c
            JOIN ( select id , fk_user_id, fk_challenge_id from tbl_user_challenge order by id DESC   ) as b
            ON c.id = b.fk_challenge_id
            where fk_user_id = '.$user_id.'
            group by b.fk_user_id');
        if($query->num_rows >0){
            return $query->row();
        }
        else
        {
            return false;
        }

    }

    public function getChallenge()
    {
        $query = $this->db->get('tbl_challenge');
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

    public function getRandomChallenge()
    {

        $query = $this->db->query('SELECT * from tbl_challenge order by RAND()');
        if($query->num_rows >0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function getChallengeByDiffCategory($lastCatID)
    {
        $query = $this->db->query('SELECT * from tbl_challenge
         where fk_category_challenge_id != '.$lastCatID.' order by RAND()');
        if($query->num_rows >0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function getChallengeEngagement()
    {
        $query = $this->db->get('vw_available_challenge_engagement');
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

    public function getChallengeEngagementAvailable($user_id, $engagement_challenge_id )
    {
         $query = $this->db->query('SELECT * from tbl_user_engagemet_challenge
         where fk_user_id = '.$user_id.' and fk_engagemet_challenge_id='.$engagement_challenge_id.' ');
        if($query->num_rows >0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function saveDataUserChallenge( $data ){
        $this->db->insert('tbl_user_challenge', $data);
        return $this->db->insert_id();
    }

     public function saveDataUserChallengeEngagement( $data ){
        $this->db->insert('tbl_user_engagemet_challenge', $data);
        return $this->db->insert_id();
    }


    public function getChallengePrize( $constraints ){
        $q = $this->db->get_where('vw_challenge_prize', $constraints );
        return count( $q->row() ) > 0 ? $q->row() : false;
    }

    public function getUserCurrentChallenge( $user_id ){

        $constraints = array(
            'fk_user_id'  => $user_id
        );

        $q = $this->db->get_where('tbl_user_current_challenge', $constraints );
        return count( $q->row() ) > 0 ? $q->row() : false;
    }

    public function getChallengeById( $challenge_id ){
        $q = $this->db->get_where('tbl_challenge', array('id' => $challenge_id) );
        return count( $q->row() ) > 0 ? $q->row() : false;
    }

    public function saveUserCurrentChallenge( $user_id, $challenge_id ){

        $this->db->insert('tbl_user_current_challenge', array(
            'fk_user_id'      => $user_id,
            'fk_challenge_id' => $challenge_id
        ));

    }

    public function getUserMom( $constraints ){
        $q = $this->db->get_where('tbl_user_mom', $constraints );
        return count( $q->row() ) > 0 ? $q->row() : false;
    }

    public function deleteUserCurrentChallenge( $user_id ){
        $this->db->where('fk_user_id', $user_id);
        $this->db->delete('tbl_user_current_challenge');
    }

    public function getRanking()
    {
      $query = $this->db->query('select @rownum:=@rownum+1 as posicion, p.* from vw_ranking p, (SELECT @rownum:=0) r WHERE p.total_points > 0 limit 20;');
        if($query->num_rows >0){
            return $query->result();
        }else{
            return false;
        }
    }


    public function getRankingByuserId($user_id)
    {
     $query = $this->db->query('SELECT * from vw_ranking ');
     $result=$query->result();

        if($query->num_rows() > 0)
        {

           foreach ($result as $key => $value)
            {
                if($value->user_id==$user_id)
                {
                    return $key + 1;
                }

           }

        }
        else
        {
            return false;
        }
    }

    public function getUserChallenges( $user_id ){
        $this->db->order_by('challenge_id', 'DESC');
        $q = $this->db->get_where('vw_user_challenge_by_category', array('user_id' => $user_id) );
        return $q->result();
    }

    public function getCountAllUserChallenge(){

        $q = $this->db->get_where('tbl_user_challenge', array('is_updated' => 0) );
        echo $this->db->last_query();
        krumo($q->result());
        return $q->result();
    }

    public function updateUserChallengeStatus(){
        $this->db->update('tbl_user_challenge', array('is_updated' => 0) );
    }

    public function getUserNoUpdatedChallenge(){

        $q = $this->db->get_where('tbl_user_challenge', array('is_updated'=> '0')  );
        $result = $q->row();

        $this->db->where('id', $result->id );
        $this->db->update('tbl_user_challenge', array('is_updated' => 1) );

        return $result;

    }

    public function updateUserChallenge( $id, $data ){

        $this->db->where('id', $id);
        $this->db->update('tbl_user_challenge', $data );
    }

    public function getPrizeSalepoints( $prize_id ){
        $q = $this->db->get_where('vw_prize_sale_points', array('prize_id' => $prize_id));
        return $q->result();
    }

    public function getPrizes(){
        $q = $this->db->get('tbl_prize');
        return $q->result();
    }

    public function getWinnersEngagementChallenge(){

        $q = $this->db->get('vw_winners_engagement_challenge');
        return $q->result();
    }

    public function updateUserData( $user_id, $data ){
        $this->db->where('id', $user_id );
        $this->db->update('tbl_user', $data );
    }

    public function getTrackableUserChallenges(){

        /* Run this before get the info
         UPDATE tbl_user_challenge uc
         JOIN tbl_user u ON u.id = uc.fk_user_id

         SET uc.facebook_long_lived_token = u.facebook_long_lived_token

         WHERE uc.facebook_long_lived_token is null;
        */

        $query = "SELECT * FROM co_madres_lg.tbl_user_challenge where facebook_long_lived_token is not null LIMIT 700,25";
        $q = $this->db->query( $query );

        return $q->result();
    }

    public function updateUsersTokens(){
        $query = "UPDATE tbl_user_challenge uc
         JOIN tbl_user u ON u.id = uc.fk_user_id

         SET uc.facebook_long_lived_token = u.facebook_long_lived_token

         WHERE uc.facebook_long_lived_token is null";

         $this->db->query( $query );
    }


}