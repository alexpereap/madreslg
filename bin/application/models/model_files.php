<?php

class Model_files extends CI_Model{

    public function upload_file($field_name, $path = './uploads/', $extensions = FALSE ){

        $config['upload_path'] = $path;
        $config['encrypt_name'] = TRUE;
        if( $extensions ){
            $config['allowed_types'] = $extensions;
        }
        $config['max_size']    = '8000';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( !$this->upload->do_upload($field_name))
        {
            // uploading failed. $error will holds the errors.
            // krumo($error);
            return array(
                'success' => FALSE,
                'error' => $this->upload->display_errors()
            );
        }
        else
        {
            $data = $this->upload->data();
            // uploading successfull, now do your further actions
            // krumo($data);

            return array(
                'success' => TRUE,
                'data' => $data
            );
        }
    }
}