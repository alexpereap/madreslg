<?php

require __DIR__ . '/../libraries/facebook-php-sdk-v4-4.0-dev/autoload.php';



use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;


session_start();

class Site extends CI_Controller {

    private $login_permissions = array('user_friends', 'email', 'publish_actions', 'user_birthday', 'user_posts');
    private $app_id = '664192710392558';
    private $app_secret = '43d3ae309ad3f5c9f76c31683cbd5d75';

    function __construct(){
      parent::__construct();

      if( intval(date('m')) > 5 && $this->router->method != 'final_winners' ){
        redirect('site/final_winners');
      }
    }

    public function final_winners(){
        setlocale(LC_ALL, 'es_ES');

        $data = array(
            'in_winners'         => true,
            'winners_engagement' => $this->main->getWinnersEngagementChallenge()
        );

        $this->load->view('header_end_activity', $data);
        $this->load->view('winners');
        $this->load->view('footer_end_activity');
    }

    public function activity_end(){

        $this->load->view('header_end_activity');
        $this->load->view('activity_end');
        $this->load->view('footer_end_activity');
    }

    public function index(){

        /*if( !$this->session->userdata('user_public') ){
            echo '<div><a href="'. $this->_get_fb_login_url() .'" >iniciar sesion</a></div>';
            echo '<div><a href="site/login" >Registrarse</a></div>';
        }*/


        $data = array(
              'login_url' => $this->_get_fb_login_url(),
              'in_home'   => true
        );

        $this->load->view('header', $data);
        $this->load->view('home');
        $this->load->view('footer');
    }

    public function login()
    {

        if( $this->session->userdata('user_public') ){

            redirect( base_url() );
            return;
        }

        FacebookSession::setDefaultApplication( $this->app_id ,  $this->app_secret );

        // login helper with redirect_uri
        $helper = new FacebookRedirectLoginHelper( base_url('site/login') );

        try {
          $session = $helper->getSessionFromRedirect();
        } catch( FacebookRequestException $ex ) {
          // When Facebook returns an error
        } catch( Exception $ex ) {
          // When validation fails or other local issues
        }

        // see if we have a session
        if ( isset( $session ) ) {


            try{


              // graph api request for user data
              $request = new FacebookRequest( $session, 'GET', '/me' );
              $response = $request->execute();

              // get response
              $graphObject = $response->getGraphObject();

              $fb_id = $graphObject->getProperty( 'id' );
              $user = $this->main->getUser( array('facebook_id' => $fb_id) );

              if( $user ){

                  // register session

                  /*
                   * Collect users with no long lived tokens
                   */

                  if( $user->facebook_long_lived_token == '' ){

                      $accessToken = $session->getAccessToken();
                      $longLivedAccessToken = $accessToken->extend();

                      $this->main->updateUserData( $user->id, array('facebook_long_lived_token' => (string) $longLivedAccessToken )  );
                  }

                  $this->_register_session( $user->id );
                  redirect( base_url() );
                  return;

              }

              // pre register proccess
              if( $user === false ){


                $accessToken = $session->getAccessToken();
                $longLivedAccessToken = $accessToken->extend();

                  $user_data = array(
                    'facebook_id'               => $fb_id,
                    'first_name'                => $graphObject->getProperty( 'first_name' ),
                    'last_name'                 => $graphObject->getProperty( 'last_name' ),
                    'birth_date'                => date('Y-m-d', strtotime($graphObject->getProperty( 'birthday' ))),
                    'email'                     => $graphObject->getProperty( 'email' ),
                    'city'                      => '',
                    'facebook_long_lived_token' => (string) $longLivedAccessToken
                  );


                  $data = array(
                    'user_data'       => $user_data,
                    'in_pre_register' => true,
                    'user_friends'    => $this->_getUserFriendList( $graphObject->getProperty( 'id' ), $session->getToken() ),
                    'login_url' => $this->_get_fb_login_url(),
                  );


                  $this->load->view('header', $data);
                  $this->load->view('register');
                  $this->load->view('footer');

              }

            }  catch( FacebookRequestException $ex ) {
              // When Facebook returns an error
              /*echo "fb<br>";
              echo "<pre>";
              print_r($ex);
              echo "</pre>";*/
            } catch( Exception $ex ) {
              // When validation fails or other local issues
              /*echo "local</br>";
              var_dump($ex);*/
            }


          } else { // not isset session
          // show login url

          $data = array(
              'login_url' => $this->_get_fb_login_url()
          );

          $this->load->view('header', $data);
          $this->load->view('register');
          $this->load->view('footer');

        }

    }

    private function _getUserFriendList($user_id, $token){


      /**
       * You should be aware the even after a review and permission to use taggable_friends, the IDs you get back can only be used to tag friends - you can't use those IDs to get any other information about a user
       * @see http://stackoverflow.com/questions/23507885/retrieve-full-list-of-friends-using-facebook-api
      */
      $url = 'https://graph.facebook.com/v2.3/' . $user_id . '/taggable_friends?access_token=' . $token . '&pretty=1&limit=5000';
      $grap_object = json_decode(file_get_contents( $url ));

      // parse data to user firends

      $user_friends = array();

      foreach( $grap_object->data as $friend ){

          $user_friends[] = array(
              'label'   => $friend->name,
              'id'      => $friend->id,
              'picture' => $friend->picture->data->url
          );
      }

      return $user_friends;
    }

    public function handleUserReg(){


      $suscribe = isset( $_POST['suscribe'] ) ? 1 : 0;

      $user_data = array(
        'facebook_id' => trim( $this->input->post('user_fb_id')),
        'document'    => trim( $this->input->post('user_document')),
        'first_name'  => trim( $this->input->post('user_firstname')),
        'last_name'   => trim( $this->input->post('user_lastname')),
        'birth_date'  => trim( $this->input->post('user_birth_year') . '-' .  $this->input->post('user_birth_month') . '-' . $this->input->post('user_birth_day')),
        'email'       => trim( $this->input->post('user_email')),
        'address'     => trim( $this->input->post('user_address')),
        'city'        => trim( $this->input->post('user_city')),
        'cellphone'   => trim( $this->input->post('user_cellphone')),
        'suscribe'    => $suscribe,
        'created_at'  => date('Y-m-d H:i:s')
      );


      $user_id = $this->main->saveUser( $user_data );

      $mom_data = array(
          'fk_user_id'  => $user_id,
          'facebook_id' => trim( $this->input->post('mom_fb_id')),
          'picture'     => trim( $this->input->post('mom_picture') ),
          'document'    => trim( $this->input->post('mom_document')),
          'first_name'  => trim( $this->input->post('mom_name')),
          'last_name'   => trim( $this->input->post('mom_lastname')),
          'email'       => trim( $this->input->post('mom_email')),
          'cellphone'   => trim( $this->input->post('mom_cellphone')),
          'created_at'  => date('Y-m-d H:i:s')
      );

      $this->main->saveUserMom( $mom_data );

      // register session
      $this->_register_session( $user_id );

      // mail sending
      $this->load->model('model_mails');
      $this->model_mails->singleEmail( $this->input->post('user_email'), $this->load->view('emails/register',  array('name' => $this->input->post('user_firstname') ) , true), 'Estas participando' );


      redirect( base_url() );
      return;
    }

    private function _register_session( $user_id ){

        $user = $this->main->getUser( array('id' => $user_id) );
        $this->session->set_userdata( 'user_public', $user );
    }

    public function logout(){
        $this->session->unset_userdata('user_public');
        redirect( base_url() );
    }

    private function _get_fb_login_url(){
        FacebookSession::setDefaultApplication( $this->app_id ,  $this->app_secret );

        // login helper with redirect_uri
        $helper = new FacebookRedirectLoginHelper( base_url('site/login') );
        return $helper->getLoginUrl( $this->login_permissions );
    }

    public function post_challenge_on_facebook(){

        $data = $this->session->userdata('facebook_post_intent');

        FacebookSession::setDefaultApplication( $this->app_id ,  $this->app_secret );

        // login helper with redirect_uri
        $helper = new FacebookRedirectLoginHelper( base_url('site/post_challenge_on_facebook') );

        // get user long lived token

        $user = $this->main->getUser( array('id' => $this->session->userdata('user_public')->id ) );
        $long_lived_token = $user->facebook_long_lived_token;

        if( $long_lived_token != '' ){
          $session = new FacebookSession($long_lived_token);
           try {
             $session->validate();
           }catch (FacebookRequestException $ex) {
             // Session not valid, Graph API returned an exception with the reason.
                try {
                  $session = $helper->getSessionFromRedirect();
                } catch( FacebookRequestException $ex ) {
                  // When Facebook returns an error
                  $this->session->set_flashdata('post_result', 'No hemos podido públicar tu imagen, por favor intentalo de nuevo.');
                  redirect( base_url('site/my_challenges') );
                } catch( Exception $ex ) {
                  // When validation fails or other local issues
                  $this->session->set_flashdata('post_result', 'No hemos podido públicar tu imagen, por favor intentalo de nuevo.');
                  redirect( base_url('site/my_challenges') );
                }
               } catch ( Exception $ex) {
                // Graph API returned info, but it may mismatch the current app or have expired.
                try {
                  $session = $helper->getSessionFromRedirect();
                } catch( FacebookRequestException $ex ) {
                  // When Facebook returns an error
                  $this->session->set_flashdata('post_result', 'No hemos podido públicar tu imagen, por favor intentalo de nuevo.');
                  redirect( base_url('site/my_challenges') );
                } catch( Exception $ex ) {
                  // When validation fails or other local issues
                  $this->session->set_flashdata('post_result', 'No hemos podido públicar tu imagen, por favor intentalo de nuevo.');
                  redirect( base_url('site/my_challenges') );
                }
          }
        } else {
            try {
              $session = $helper->getSessionFromRedirect();
            } catch( FacebookRequestException $ex ) {
              // When Facebook returns an error
              $this->session->set_flashdata('post_result', 'No hemos podido públicar tu imagen, por favor intentalo de nuevo.');
              redirect( base_url('site/my_challenges') );
            } catch( Exception $ex ) {
              // When validation fails or other local issues
              $this->session->set_flashdata('post_result', 'No hemos podido públicar tu imagen, por favor intentalo de nuevo.');
              redirect( base_url('site/my_challenges') );
            }
        }


        // see if we have a session
        if ( isset( $session ) ) {

          // generates and stores a long lived token
          if( $long_lived_token == '' ){
              $accessToken = $session->getAccessToken();
              $longLivedAccessToken = $accessToken->extend();
              $this->main->updateUserData( $user->id, array('facebook_long_lived_token' => (string) $longLivedAccessToken )  );
              $long_lived_token = (string) $longLivedAccessToken;
          }

          try{
              $post_req_data =  array(
                'message'      => $data['message'],
                'url'          => base_url('uploads/' . $data['file_name'] )
              );

              // if the mom has a facebook id (meaning the user registerd his/her mom by facebook)
              /*if( $data['user_mom']->facebook_id != '' ){
                  $tag_mom = array(
                    'tag_uid'  => $data['user_mom']->facebook_id,
                    'tag_text' => $data['user_mom']->first_name . ' ' . $data['user_mom']->last_name,
                    'x'        => 0,
                    'y'        => 0
                  );

                  $tags = array($tag_mom);
                  $post_req_data['tags'] = $tags;
              }*/

                // Publish to User’s Timeline
              $request = ( new FacebookRequest( $session, 'POST', '/me/photos', $post_req_data ))->execute();

              // Get response as an array, returns ID of post
              $response = $request->getGraphObject()->asArray();

              // success on facebook post
              $this->_handleFacebookPostSuccess( $response, $long_lived_token );
          } catch( FacebookRequestException $ex ) {
            // When Facebook returns an error
            $this->session->set_flashdata('post_result', 'No hemos podido públicar tu imagen, por favor intentalo de nuevo.');
            redirect( base_url('site/my_challenges') );
          } catch( Exception $ex ) {
            // When validation fails or other local issues
            $this->session->set_flashdata('post_result', 'No hemos podido públicar tu imagen, por favor intentalo de nuevo.');
            redirect( base_url('site/my_challenges') );
          }

        } else {

            redirect( $helper->getLoginUrl( $this->login_permissions ) );
        }
    }


    private function _getUserCurrentChallenge( $user_id ){
      /*
       * checks if this user already has a challenge assigned, if that is the chase until he doesn't completes
       * this challenge he can't make another challenge
       */

      $current_challenge = $this->main->getUserCurrentChallenge( $user_id );

      if( $current_challenge ){
        return $this->main->getChallengePrize( array('challenge_id' => $current_challenge->fk_challenge_id) );
      }

      return false;
    }

    private function _getChallengeByUserID($user_id)
    {

      $availableChallenge=$this->main->getAvailableChallenges($user_id);

      if($availableChallenge)
      {

        // gets last challenged completed by user
        $lastChallengeComplete=$this->main->getUserLastChallengeCompleted($user_id);


        // if such a challenge exists tries to get a challenge with different category
        // other wise just get a random challenge
        if($lastChallengeComplete)
        {

          $lastCatID=$lastChallengeComplete->fk_category_challenge_id;
          $challenge=$this->main->getChallengeByDiffCategory($lastCatID);
          if( $challenge )
          {
            return $challenge;
          }

          else
          {
            $challenge=$this->main->getRandomChallenge();
            return $challenge;
          }
        }
        else
        {
          $challenge=$this->main->getRandomChallenge();
          return $challenge;
        }

      }
      else
      {
        // echo "no hay retos disponibles para este usuario";
        return FALSE;
      }
      //$challengeComplete=$this->main->getUserLastChallengeCompleted( array('id' => $availableChallenge->user_id) );
    }

    private function _getChallengeEngagement($user_id)
    {


      $challenge_engagement = $this->main->getChallengeEngagement();

      if( $challenge_engagement ){

        $challenge_engagement_available=$this->main->getChallengeEngagementAvailable($user_id, $challenge_engagement->id );
        if($challenge_engagement_available)
        {
          // echo "Ya haz realizado tu reto diario";
          return false;
        }
        else
        {

         return $challenge_engagement;
        }
      } else {
         // no challenge availables
        return false;
      }


   }

   public function saveUserChallenge()
   {

    $user_data = array(
        'fk_user_id' => trim( $this->input->post('user_id')),
        'fk_challenge_id'    => trim( $this->input->post('challenge_id')),
        'challenge_photo'  => trim( $this->input->post('picture')),
        'created_at'  => date('Y-m-d H:i:s')
      );


      $user_id = $this->main->saveDataUserChallenge( $user_data );
   }

   public function saveUserChallengeEngagement()
   {
    $user_data = array(
      'fk_user_id' => trim( $this->input->post('user_id')),
      'fk_engagemet_challenge_id'    => trim( $this->input->post('challenge_id')),
      'challenge_photo'  => trim( $this->input->post('picture')),
      'created_at'  => date('Y-m-d H:i:s')
      );


      $user_id = $this->main->saveDataUserChallengeEngagement( $user_data );
   }

   public function setPostUpdateProcess(){


      // $this->main->updateUserChallengeStatus();
      $this->session->set_userdata( 'facebook_tracker_iterations', 39 );
      krumo($this->main->getCountAllUserChallenge());

   }


    public function experimental_fb_tracker(){

          $this->main->updateUsersTokens();

          FacebookSession::setDefaultApplication( $this->app_id ,  $this->app_secret );

          $user_challenges = $this->main->getTrackableUserChallenges();

          foreach(  $user_challenges  as $uc ){


              $long_lived_token = $uc->facebook_long_lived_token;

              try {
                  $session = new FacebookSession($long_lived_token);
              } catch( FacebookRequestException $ex ) {
                // When Facebook returns an error
                echo "unable to init session register id :" . $uc->id . "error: " . $ex . "<br/>";

              } catch( Exception $ex ) {
                // When validation fails or other local issues
                echo "unable to init session id :" . $uc->id . "error: " . $ex . "<br/>";

              }

              if ( isset( $session ) ) {

                  $token = $long_lived_token;

                  // GET LIKES
                  $url_likes = 'https://graph.facebook.com/v2.3/' . $uc->facebook_id . '/likes?access_token=' . $token . '&format=json&limit=5000&method=get&pretty=1';

                  @$grap_object = file_get_contents( $url_likes );
                  $grap_object = json_decode( $grap_object );

                  $total_likes = count( $grap_object->data );

                  // GET SHARES
                  $url_shares = 'https://graph.facebook.com/v2.3/' . $uc->facebook_id . '/sharedposts?access_token=' . $token . '&format=json&limit=5000&method=get&pretty=1';

                  @$grap_object = file_get_contents( $url_shares );
                  $grap_object = json_decode( $grap_object );

                  $total_shares = count( $grap_object->data );

                  $update_data = array(
                      'likes'  => $total_likes,
                      'shares' => $total_shares
                  );


                  $this->main->updateUserChallenge( $uc->id, $update_data );

              }

              sleep(1);
          } // end foreach
    }

    public function engagement_challenge(){

        if( !$this->session->userdata('user_public') ){

            redirect( base_url('site/login') );
            return;
        }

        $engagement_challenge = $this->_getChallengeEngagement( $this->session->userdata('user_public')->id  );

        $data = array(
              'login_url'            => $this->_get_fb_login_url(),
              'engagement_challenge' => $engagement_challenge
        );

        $this->load->view('header', $data);
        $this->load->view('engagement_challenge');
        $this->load->view('footer');
    }


    public function submit_engagement_challenge(){


      if(  $_FILES['engagementChallengeFile']['name'] != '' ){


            $rf = $this->model_files->upload_file('engagementChallengeFile', './uploads', 'jpg|jpeg|png' );

            if( $rf['success'] === true ){

                $file_name = $rf['data']['file_name'];

                $engage_challenge_data = array(
                  'fk_user_id' => trim( $this->session->userdata('user_public')->id ),
                  'fk_engagemet_challenge_id'  => trim( $this->input->post('engagement_challenge_id')),
                  'challenge_photo'  => trim( $file_name ),
                  'created_at'  => date('Y-m-d H:i:s')
                );

                $this->main->saveDataUserChallengeEngagement( $engage_challenge_data );


                $this->session->set_flashdata('success', 'Tu reto se ha guardado exitosamente.');
                $this->session->set_flashdata('success_title', 'Felicidades');

                redirect(base_url('site/engagement_challenge'));
            } else {
                // redirect( base_url() . 'site/set_purchase_data?error=' . $rf['error'] );
                // krumo($rf['error']);

                $this->session->set_flashdata('success_title', 'Ha ocurrido un problema');
                $this->session->set_flashdata('success', 'Lo sentimos, no hemos podido procesar tu imagen.<br/><ul><li>Verifica que tu imagen sea de formato .jpg, .jpeg o .png.</li><li>Guardala tu imagen en alguno de estos formatos y verifica que no pese más de 7MB.</li> Intenta subir tu reto de nuevo haciendo click <a href="' . base_url('site/engagement_challenge') . '" >aquí</a>');
                redirect(base_url('site/engagement_challenge'));
            }

        } else {

            $this->session->set_flashdata('success_title', 'Ha ocurrido un error');
            $this->session->set_flashdata('success', 'Intenta subir tu reto de nuevo haciendo click <a href="' . base_url('site/engagement_challenge') . '" >aquí</a>');
            redirect(base_url('site/engagement_challenge'));
        }
    }

    public function my_challenges(){


        if( !$this->session->userdata('user_public') ){

            redirect( base_url('site/login') );
            return;
        }

        /*$challenge = $this->_getChallengeByUserID( $this->session->userdata('user_public')->id );
        $challenge = $this->main->getChallengePrize( array('challenge_id' => $challenge->id ) );*/

        // http://graph.facebook.com/fb_user_id/picture?width=100&height=100


        /**
         * @param (BOOL) current_challenge => checks if user has a challenge assigned
         * @param available_challenge => if false the user can't generate challenges
         */
        $data = array(
            'in_my_challenges'     => true,
            'current_challenge'    => $this->_getUserCurrentChallenge( $this->session->userdata('user_public')->id ),
            'available_challenge'  => $this->main->getAvailableChallenges(  $this->session->userdata('user_public')->id),
            'user_mom'             => $this->main->getUserMom( array('fk_user_id' => $this->session->userdata('user_public')->id) ),
            'completed_challenges' => $this->main->getUserChallenges( $this->session->userdata('user_public')->id ),
            'user_rank'            => $this->main->getRankingByuserId( $this->session->userdata('user_public')->id ),
            'login_url' => $this->_get_fb_login_url()
        );


        if( $data['current_challenge'] ){

            $data['prize_salepoints'] = $this->main->getPrizeSalepoints( $data['current_challenge']->prize_id );
        }

        $this->load->view('header', $data);
        $this->load->view('my_challenges');
        $this->load->view('footer');

    }


    public function ajax_getChallengeByUserID(){
        // echo json_encode( $this->_getChallengeByUserID( $this->session->userdata('user_public')->id ) );

        $challenge = $this->_getChallengeByUserID( $this->session->userdata('user_public')->id );
        $this->main->saveUserCurrentChallenge( $this->session->userdata('user_public')->id, $challenge->id );

        $data = array(
            'current_challenge'   => $this->_getUserCurrentChallenge( $this->session->userdata('user_public')->id ),
            'available_challenge' => $this->main->getAvailableChallenges(  $this->session->userdata('user_public')->id)
        );

        if( $data['current_challenge'] ){

            $data['prize_salepoints'] = $this->main->getPrizeSalepoints( $data['current_challenge']->prize_id );
        }

        echo $this->load->view('gen_challenge', $data);
    }

    public function submit_challenge(){



        if(  $_FILES['challengeFile']['name'] != '' ){


            $rf = $this->model_files->upload_file('challengeFile', './uploads', 'jpg|jpeg|png' );

            if( $rf['success'] === true ){

                $file_name = $rf['data']['file_name'];

                // krumo($file_name);

                // FACEBOOK POST :@

                $user_current_challenge = $this->main->getUserCurrentChallenge( $this->session->userdata('user_public')->id );
                $challenge = $this->main->getChallengeById( $user_current_challenge->fk_challenge_id );

                $facebook_post_data = array(
                    'file_name' => $file_name,
                    'user_mom'  => $this->main->getUserMom( array('fk_user_id' => $this->session->userdata('user_public')->id) ),
                    'message'   => $challenge->challenge_description . ' - #test_YoQuieroAMamáconLG'
                );

                // temp fix fb approval
                $facebook_post_data['message'] = $this->input->post('challengeText');

                $this->session->set_userdata( 'facebook_post_intent', $facebook_post_data );

                // krumo($facebook_post_data);

                $this->post_challenge_on_facebook( $facebook_post_data );

                /*$this->session->set_flashdata('success', 'Tu reto se ha guardado exitosamente.');
                $this->session->set_flashdata('success_title', 'Felicidades');

                redirect(base_url('site/engagement_challenge'));*/
            } else {

              $this->session->set_flashdata('post_result', 'No hemos podido públicar tu imagen, por favor intentalo de nuevo.');
              redirect( base_url('site/my_challenges') );

            }

        } else {

           $this->session->set_flashdata('post_result', 'No hemos podido públicar tu imagen, por favor intentalo de nuevo.');
           redirect( base_url('site/my_challenges') );
        }

    }


    private function _handleFacebookPostSuccess( $fb_response, $long_lived_token ){

        $fb_post_data = $this->session->userdata('facebook_post_intent');
        $user_current_challenge = $this->main->getUserCurrentChallenge( $this->session->userdata('user_public')->id );

        $user_challenge_data = array(
            'fk_user_id'       => $this->session->userdata('user_public')->id,
            'fk_challenge_id'  => $user_current_challenge->fk_challenge_id,
            'challenge_photo'  => $fb_post_data['file_name'],
            'facebook_id'      => $fb_response['id'],
            'facebook_post_id' => $fb_response['post_id'],
            'created_at'       => date('Y-m-d H:i:s'),
            'facebook_long_lived_token' => $long_lived_token
        );

        $this->main->saveDataUserChallenge( $user_challenge_data );
        $this->main->deleteUserCurrentChallenge( $this->session->userdata('user_public')->id );

        $this->session->set_flashdata('post_result', 'Tu reto se ha públicado exitosamente.');
        redirect( base_url('site/my_challenges') );

    }


    public function Ranking()
    {
      $Ranking=$this->main->getRanking();

      $premios= array('Televisor LG Ref:  55LB650T',
        'Lavadora LG Ref: WFS1439EPD',
        'Nevera LG Ref: GT46SGP',
        'PC LG Ref: Tab book 11T540',
        'Celular LG Ref G3',
        'Barra de sonido LG Ref LAS750.'
        );

      $data_ranking = array(
              'ranking' => $Ranking,
              'prizes' => $premios,
              'in_ranking' => true,
              'login_url' => $this->_get_fb_login_url()
      );

      /*krumo($data_ranking);
      die();*/

        $this->load->view('header', $data_ranking);
        $this->load->view('ranking');
        $this->load->view('footer');
    }

    public function RankingByUserId($user_id)
    {

     $RankingUserId=$this->main->getRankingByuserId($user_id);

       $data_rankingByUserId = $RankingUserId;
       krumo($data_rankingByUserId);

    }

    public function mechanical(){

        $data = array(
            'in_mechanical' => true,
            'login_url' => $this->_get_fb_login_url()
        );

        $this->load->view('header', $data);
        $this->load->view('mechanical');
        $this->load->view('footer');
    }

    public function prizes(){

       $prizes = $this->main->getPrizes();

       foreach( $prizes as $key => $p ){
          $prizes[$key]->salepoints = $this->main->getPrizeSalepoints( $p->id );
       }

       $category_prizes_ga = array("ga('send','event', 'Catégoria-P1-Tv','Click','/Botón-Conoce-tu-reto');",
        "ga('send','event', 'Catégoria-P2-Lavadora','Click','/Botón-Conoce-tu-reto');",
        "ga('send','event', 'Catégoria-P3-Nevera','Click','/Botón-Conoce-tu-reto');",
        "ga('send','event', 'Catégoria-P4-PC','Click','/Botón-Conoce-tu-reto');",
        "ga('send','event', 'Catégoria-P5-Smartphone','Click','/Botón-Conoce-tu-reto');",
        "ga('send','event', 'Catégoria-P6-Barra-Sonido','Click','/Botón-Conoce-tu-reto');"
      );

       $category_prizes_idx = array('Catégoria-P1-Tv','Catégoria-P2-Lavadora','Catégoria-P3-Nevera',
        'Catégoria-P4-PC','Catégoria-P5-Smartphone','Catégoria-P6-Barra-Sonido'
      );

        $data = array(
            'in_prizes'           => true,
            'login_url'           => $this->_get_fb_login_url(),
            'prizes'              => $prizes,
            'category_prizes_ga'  => $category_prizes_ga,
            'category_prizes_idx' => $category_prizes_idx
        );

        $this->load->view('header', $data);
        $this->load->view('prizes');
        $this->load->view('footer');
    }

    public function phpinfo(){
      phpinfo();
    }

    public function reg_email_preview(){
        $this->load->view('emails/register');
    }

    public function winners(){

        setlocale(LC_ALL, 'es_ES');

        $data = array(
            'in_winners'         => true,
            'winners_engagement' => $this->main->getWinnersEngagementChallenge()
        );

        $this->load->view('header', $data);
        $this->load->view('winners');
        $this->load->view('footer');
    }


  }



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */