/**
 * sg-scripts.js
 */
(function (document, undefined) {
  "use strict";

  // Add js class to body
  document.getElementsByTagName('body')[0].className+=' js';


  // Add functionality to toggle classes on elements
  var hasClass = function (el, cl) {
      var regex = new RegExp('(?:\\s|^)' + cl + '(?:\\s|$)');
      return !!el.className.match(regex);
  },

  addClass = function (el, cl) {
      el.className += ' ' + cl;
  },

  removeClass = function (el, cl) {
      var regex = new RegExp('(?:\\s|^)' + cl + '(?:\\s|$)');
      el.className = el.className.replace(regex, ' ');
  },

  toggleClass = function (el, cl) {
      hasClass(el, cl) ? removeClass(el, cl) : addClass(el, cl);
  };

  var selectText = function(text) {
      var doc = document;
      if (doc.body.createTextRange) {
          var range = doc.body.createTextRange();
          range.moveToElementText(text);
          range.select();
      } else if (window.getSelection) {
          var selection = window.getSelection();
          var range = doc.createRange();
          range.selectNodeContents(text);
          selection.removeAllRanges();
          selection.addRange(range);
      }
  };


  // Cut the mustard
  if ( !Array.prototype.forEach ) {
    
    // Add legacy class for older browsers
    document.getElementsByTagName('body')[0].className+=' legacy';

  } else {

    // View Source Toggle
    [].forEach.call( document.querySelectorAll('.sg-btn--source'), function(el) {
      el.onclick = function() {
        var that = this;
        var sourceCode = that.parentNode.nextElementSibling;
        toggleClass(sourceCode, 'is-active');
        return false;
      };
    }, false);

    // Select Code Button
    [].forEach.call( document.querySelectorAll('.sg-btn--select'), function(el) {
      el.onclick = function() {
        selectText(this.nextSibling);
        toggleClass(this, 'is-active');
        return false;
      };
    }, false);
  }

  
  // Add operamini class to body
  if (window.operamini) {
    document.getElementsByTagName('body')[0].className+=' operamini';    
  } 
  // Opera Mini has trouble with these enhancements
  // So we'll make sure they don't get them
  else {
    // Init prettyprint
    prettyPrint();
  
  }
 
 })(document);

/*READY COMMONS*/
  jQuery(document).ready(function($) { /*"use strict";*/
    /*LG MIN-PARALLAX HOME*/
      if(jQuery(".sg-homeIntro")[0] || jQuery(".sg-homeIntro").lenght > 0){
         jQuery(window).scroll(function () {
          //CHECK BEFORE FROM MOBILE
              if(jQuery(window).width() > 767){
                var scrollTop = jQuery(window).scrollTop();
                var height = jQuery(window).height();
                var opacity = ((height - scrollTop) / height);
                jQuery('.sg-body.sg-homeIntro').css({
                    'opacity': opacity, 'top':(70+((height - scrollTop) / height)*60.5)
                });
                var theOpcHdr = ( opacity > 1.2 || scrollTop > 600  ? theOpcHdr = 0.85 : (opacity/2.5) );
                jQuery('body.lg-mothers .navbar-fixed-top').css({ 'background-color': 'rgba(255,255,255,'+(theOpcHdr)+')' });
              }else{
                //console.log('is mobile');
                jQuery('.navbar.navbar-default').removeClass('navbar-fixed-top');
                jQuery('body.lg-mothers.fixBg').addClass('toTop');
              }
          });
      };    
      /*
      var height = jQuery(window).height();
            height = height  + jQuery('.sg-body.sg-intCont.leftSid').height() * 2;
            jQuery('.sg-body.sg-intCont.leftSid').css({
                'opacity': ((height - scrollTop) / height)
            }); */
      
      //*TWEEN ICONS CHALLENGE*//
      jQuery("#tweenToChallenge").click(function(event){
          event.preventDefault();
          event.stopPropagation();
            jQuery('#defHddn').delay(220).queue('fx', function() { jQuery(this).addClass('vsbl'); });
            //jQuery('#defHddn').delay(500).addClass('vsbl');
            jQuery('#twnCllng .thMchnTwnn').addClass('tweened');
            setTimeout(function(){
                //jQuery('#defHddn').delay(220).queue('fx', function() { jQuery(this).removeClass('vsbl'); });
                jQuery("#defHddn").removeClass().dequeue();
                jQuery('#twnCllng .thMchnTwnn').removeClass('tweened');
                //CALL THE FUNCTION TO GET CHALLENGE
                  /*add active icon class IN SPECIFIC CASE 
                    array('iconChll-audio','iconChll-lavadora','iconChll-movil','iconChll-nevera','iconChll-pc','iconChll-tv')*/
                  //-->
            }, 3500);
            
      });


      //*CAROUSEL LISTENERS*//
      jQuery('#carousel-prizes').on('slid.bs.carousel', function () {
        // This variable contains all kinds of data and methods related to the carousel
        var carouselData = jQuery(this).data('bs.carousel');
        //var currentIndex = carouselData.getActiveIndex();
        var currentIndex = carouselData.getItemIndex(carouselData.$element.find('.item.active'));
        var total = carouselData.$items.length;
        // Create the text we want to display.
        // We increment the index because humans don't count like machines
        var text = (currentIndex + 1)/* + " of " + total*/;
        jQuery('#carousel-prizes .icoPos span').text(text);
        //console.log(text);
      });

      /*LG HEADER STICK*/  
        /*var makeStickIntHeads =*/ function makeStickIntHeads(){   
          if(jQuery(window).width() > 767 && jQuery('.sg-body.sg-headCont')[0]){
            //console.log('function');
            var headIntPos = jQuery('.sg-body.sg-headCont').offset(), topHeight =  jQuery('.navbar.navbar-fixed-top').height();
                jQuery(window).scroll(function(){
                  if(jQuery(window).scrollTop() > (headIntPos.top - 10) ){
                     //jQuery('.sg-body.sg-headCont').css('position','fixed').css('top','0');
                     jQuery('.navbar.navbar-fixed-top').css('margin-bottom', (topHeight/0.8)+'px');
                     jQuery('.sg-body.sg-headCont').addClass('fixed blnkOpct');
                     jQuery('.lg-mothers.toTop').css('margin-top', (topHeight/4.2)+'px');
                  } else {
                      //jQuery('.sg-body.sg-headCont').css('position','static');
                      jQuery('.navbar.navbar-fixed-top').css('margin-bottom', '20px');
                      jQuery('.sg-body.sg-headCont').removeClass('fixed blnkOpct');
                      jQuery('.lg-mothers.toTop').css('margin-top', '0');
                  }    
                });
              //console.log('trigged');
          }else if(jQuery(".sg-homeIntro")[0] && jQuery(window).width() < 767) {
              //console.log('is mobile');
                jQuery('.navbar.navbar-default').removeClass('navbar-fixed-top');
                jQuery('body.lg-mothers.fixBg').addClass('toTop');
          };
        }/*()*/; makeStickIntHeads();

    jQuery(window).on("resize", makeStickIntHeads);

    /*TRIGGER RESIZE FUNCTION*/
      jQuery(window).on('resize',function() {
          //console.log('resize');
          makeStickIntHeads();        
      }).trigger('resize');

  });